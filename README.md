# Timeless Timing Attacks and Preload Defenses in Tor's DNS Cache

[To appear][] in USENIX Security (2023).

Limitations in our artifacts:

- Out of an abundance of caution, as described in the paper, we have removed
  the counters for normal Alexa and Tranco lists in our released dataset.

- For the preload list generation, we had a minor (phew) bug where the extended
  list generation was off by one, making the extended lists off by one base
  site. This has no meaningful impact on the results, but we wanted to point it
  out for sake of transparency. Found by Daniel Bahmiary as part of his thesis
  work around improving preload list generation.

- There's an errata in Section 5.1: the interval should be `[t+x-clipped_ttl,
  t]`, where `t` is the _website visit_ time (observed), `x` is the duration
  that the domain was cached (measured), and `clipped_ttl` is either 5m or 60m
  (determined based on the original TTL).  This interval also assumes that the
  fuzzy TTL is sampled from `[m, clipped_ttl]`, as described earlier in the same
  paragraph.  The mitigation that has been merged in Tor uses a slightly
  different [formula][] for computing fuzzy TTLs that is less restrictive,
  essentially adding +-4m to the (deterministically clipped) TTLs by sampling
  uniformly at random.

[To appear]: https://www.usenix.org/conference/usenixsecurity23/presentation/dahlberg
[formula]: https://gitlab.torproject.org/tpo/core/tor/-/blob/tor-0.4.7.12/src/core/or/connection_edge.c#L488-501
