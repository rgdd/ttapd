\section{Tor's DNS Cache Today} \label{sec:torcache}

To better understand the DNS cache of Tor today, we set out to collect
performance metrics from exits in the live Tor network.
Section~\ref{sec:torcache:ethics} covers ethical considerations, followed by
data collection in Section~\ref{sec:torcache:collection} and resulting metrics
in Section~\ref{sec:torcache:metrics}.

\subsection{Ethical Considerations} \label{sec:torcache:ethics}

We submitted a proposal to the Tor Research Safety Board describing measurements
that would ultimately inform the design of a long-term defense
(Section~\ref{sec:long}) against our improved attack (Section~\ref{sec:attack}).
To be able to assess the impact of the defense we needed to better understand
the DNS cache Tor has today as a baseline. After a couple of iterations with the
Tor Research Safety Board we reached consensus, and then successfully completed
our university's ethical review process. The proposal also included measurements
needed for our defense, described later in
Section~\ref{sec:long:preload:collection}. During the measurements period of
four months we consulted the Tor Research Safety Board to discuss our results.

The intuition of our measurement is as follows.  Two exit relays are operated to
collect counters related to domain lookups.  For example, the number of lookups
and cache hits (Section~\ref{sec:torcache:collection}). These counters are the
result of all traffic at the exit, aggregated over 15 minutes intervals before
being written to disk and then reset in memory. Based on an exit probability of
about $0.0005$ ($\approx 100$Mbit/s), we extrapolated from the measurements of
Mani \emph{et~al.}~\cite{ManiWJJS18} that we should expect about 725 website
visits during 15 minutes.  Each website visit typically triggers multiple domain
lookups~\cite{GreschbachPRWF17} that affect our global counters.  A collection
interval of 15 minutes should thus aggregate hundreds of website visits for a
small fraction of the network, making the resulting dataset hardly useful for an
attacker performing correlation or confirmation attacks on the network.
This sketch appears to be confirmed by our measurement results: out of 23,632
15-minute intervals, only 18 contained less than 1,000 lookups.  
Our conclusion together with the Tor Research Safety Board was that the
resulting dataset should be safe to make public (further discussed later).

\subsection{Data Collection} \label{sec:torcache:collection}

Two 100 Mbit/s exit relays were operated on the premises of DFRI
(\texttt{https://www.dfri.se}) from May 2 until September 3, 2022.  One exit was
configured in its exit policy
with \emph{web} ports\footnote{%
	Reject all ports except 80 and 443.  (The exit can still do DNS for users.)
}. The other relay was configured with
\emph{permissive} ports\footnote{%
	Allow all ports except 25, 119, 135--139, 445, 563, 1214, 4661--4666,
	6346--6429 6699, and 6881--6999.
} to also allow non-web traffic.  Otherwise the two exits were identical,
running on the same VM with a dedicated \texttt{unbound} process that had
caching disabled by setting the \texttt{rrset-cache-size} to zero (to avoid TTL
biases). We collected the following counters every 15 minutes at both exits:

\begin{description}
	\item[timestamp] UNIX timestamp when the data was collected.
	\item[lookups] Total number of observed domain lookups.
	\item[hits\_5m] Number of cache hits with a TTL of 300 seconds.
	\item[hits\_60m] Number of cache hits with a TTL of 3,600 seconds.
	\item[hits\_pending] Number of cache hits with a pending resolve, i.e.,
		an answer has been requested but is not yet available.
	\item[hits\_same\_circuit] Number of streams that looked up a domain
		that was previously looked up on the same circuit. 
	\item[num\_cache\_entries] Number of entries in Tor's DNS cache.
\end{description}

A timestamp is needed to plot metrics as a function of time.  Timestamps are
also crucial for the additional counters described in
Section~\ref{sec:long:preload:collection}.  The number of lookups and different
types of cache hits are needed to get a baseline of cache-hit ratios.  The
number of entries in Tor's DNS cache (at the time of collection) is needed to
get a baseline of memory usage. The necessary Tor changes to collect all metrics
(including Section~\ref{sec:long:preload:collection}) were relatively modest:
400 lines of code.

\subsection{Metrics} \label{sec:torcache:metrics}

Regarding lookups per 15 minutes, the web exit processed a mean of 17,530 and
median of 13,393 lookups (Figure~\ref{fig:metrics:lookweb}), and the permissive
exit processed a mean of 41,100 and median of 26,940 lookups
(Figure~\ref{fig:metrics:lookperm}). The permissive exit policy results in
significantly more lookups. Around August 1, our exits experienced downtime,
visible as dips in lookups in both figures (at times fewer than 1,000 lookups,
as noted in Section~\ref{sec:torcache:ethics}). Exit probability is weakly
correlated with lookups: Pearson correlation 0.30 (web) and 0.16 (permissive).

\begin{figure}[ht]
	\centering
	\subfloat[][web]{%
		\includegraphics[width=.9\columnwidth]{img/plot_lookups-web.pdf}
		\label{fig:metrics:lookweb}
	}\\
	\subfloat[][permissive]{%
		\includegraphics[width=.9\columnwidth]{img/plot_lookups-permissive.pdf}
		\label{fig:metrics:lookperm}
	}
	\caption{%
	Lookups every 15 minutes and exit probability.}
	\label{fig:lookups}
\end{figure}

Figures~\ref{fig:metrics:cacheweb} and~\ref{fig:metrics:cacheperm} show the
number of entries in Tor's DNS cache.  The web exit has a mean of 7,672 and
median of 7,325 entries, and the permissive exit a mean of 12,130 and median of
11,408 entries. Both appear relatively stable compared to the number of lookups
(note log-scale y-axis in Figure~\ref{fig:lookups}).  Likely, this is
because traffic on the Tor network is not uniformly distributed, but rather
concentrated to relatively few destinations, e.g., as shown with website
popularity~\cite{ManiWJJS18}.

\begin{figure}[ht]
	\centering
	\subfloat[][web]{%
		\includegraphics[width=.85\columnwidth]{img/plot_cache_entries-web.pdf}
		\label{fig:metrics:cacheweb}
	}\\
	\subfloat[][permissive]{%
		\includegraphics[width=.85\columnwidth]{img/plot_cache_entries-permissive.pdf}
		\label{fig:metrics:cacheperm}
	}
	\caption{%
	Cache entries every 15 minutes.}
	\label{fig:cache}
\end{figure}

Central to a DNS cache is its \emph{cache-hit} ratio: how often lookups can be
resolved using cached entries instead of asking DNS resolvers.
Figures~\ref{fig:metrics:hitsweb} and~\ref{fig:metrics:hitsperm} show the
cache-hit ratios for the two exits, with a mean cache-hit ratio of 0.80 (web)
and 0.83 (permissive). We also show if the cache hits occurred due to a cache
entry used earlier on the same circuit (``same'') or from another circuit
(``shared''). Further, over all the cache hits, we show if the hits were because
of DNS entries with a five-minute cached TTL (``5min''), a 60-minute cached TTL
(``60min''), or pending entries in the DNS cache (``pending''). Same circuit
hits are likely due to Tor Browser improving performance by creating multiple
streams to the same destination. The cross-circuit cache-hit ratio is much
smaller (``shared'') with a mean of 0.11 (web) and 0.17 (permissive). We return
to these ratios in Section~\ref{sec:long:evaluation} to compare with our
defense.

\begin{figure}[ht]
	\centering
	\subfloat[][web]{%
	\includegraphics[width=1.0\columnwidth]{img/plot_cache_hits-web.pdf}
	\label{fig:metrics:hitsweb}
	}\\
	\subfloat[][permissive]{%
	\includegraphics[width=1.0\columnwidth]{img/plot_cache_hits-permissive.pdf}
	\label{fig:metrics:hitsperm}
	}
	\caption{%
		Cache-hit ratio every 15 minutes. The total ratio can be split
		by same+shared hits or 60min+5min+pending hits.
	}
	\label{fig:hits}
\end{figure}

During the four months of measurements, our exits experienced sporadic downtime
(early August) and the Tor-network endured significant network DDoS
activities~\cite{network-ddos}. This shows in our data, e.g., with the drop to
close to zero lookups in Figure~\ref{fig:lookups}, huge spikes of cached entries
in Figure~\ref{fig:cache}, and periods where the cache-hit ratio was almost
one in Figure~\ref{fig:hits}.

To summarize, Tor's DNS cache has a cache-hit ratio over 80\% using a modestly
sized DNS cache.  About 11--17\% of these hits are due to sharing the cache
across circuits.  The number of lookups are weakly correlated to exit
probability.
