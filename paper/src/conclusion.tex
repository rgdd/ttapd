\section{Conclusion} \label{sec:conclusion} 
Our timeless timing attack on Tor's DNS cache is virtually perfect,
significantly improving over earlier timing attacks~\cite{wfwo}. Based on 12
million measurements in the live Tor network, we only observed a 0.00025 failure
rate due to vanished circuits and other transient networking errors that are
easy to account for. We responsibly disclosed the attack to the Tor Project
and coordinated the process around defenses with them.

Our proposed mitigations are just that---mitigations---and do not completely
address the underlying issues. The fuzzy TTLs mitigation primarily addresses
confirmation with WF attacks involving moderately popular domains. Cover
lookups, while valuable if done, does not scale and requires continuous efforts
that are not easily automated on a large scale.

Setting out to find long-term solutions, we landed in redesigning Tor's DNS
cache completely with a preload design. To inform the design and to evaluate its
feasibility, we ran a four-month experiment starting in May 2022 measuring key
performance metrics. To ensure that our measurements were safe, we repeatedly
consulted the Tor Research Safety Board and completed our university ethical
review process. We received positive feedback as well as invaluable suggestions
along the way to minimize any potential harm to the Tor network and its users.

First, the preload design is immune to timing and timeless attacks due to never
sharing any data in the DNS cache injected due to user activity across circuits.
Secondly, the preload lists of domains based on extended Alexa, extended Tranco,
and Cisco Umbrella all show impressive cache-hit ratios. Depending on list, it
is possible to get comparable cache-hit ratios, memory usage, and resolver load
as Tor today. More extensive lists can trade modest increases in memory and
resolver load with significantly higher cache-hit ratios, especially for web
traffic. Important future work is improving how the extended lists are
generated---e.g., by tailoring them specifically for relays in certain regions
(location sensitivity), excluding unique tracking domains, or crawling websites
to discover subdomains---which is likely to lead to higher cache-hit ratios and
smaller lists.

One of the biggest downsides of the preload design is that the most effective
preload lists are extended lists based on Alexa or Tranco, requiring continuous
efforts to update. Fortunately, our measurements show that even four-month-old
extended lists remain effective with significant improvement over baseline Tor.
It is likely feasible for the Tor Project to generate and ship hard-coded
preload lists as part of regular Tor releases and still improve performance
compared to today.

Like Mani \emph{et~al.}~\cite{ManiWJJS18}, we see that traffic in the Tor
network appears to reasonably match website/domain popularity lists like Alexa,
Tranco, and Umbrella. This is fundamental for the preload design, and likely
also a contributing factor for the observed long stability of the extended
preload lists, since the most popular sites see relatively little
churn~\cite{PochatGJ19}. Finally, our measurements indicate that the Tor network
has grown by about 300\% in terms of number of streams since 2018, and that the
large majority of Tor's current DNS caching is a privacy harm rather than a
cross-circuit performance boost.
