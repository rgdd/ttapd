\section{Introduction} \label{sec:introduction}
Tor~\cite{tor} is a volunteer-operated anonymity network composed of relays that
route encrypted traffic with low latency.
One of Tor's trade-offs is to not provide anonymity against a global passive
attacker that observes traffic as it enters and leaves the
network~\cite{tor,trilemma}.
A typical attacker setting is therefore to only observe encrypted traffic as it
enters the network from an identifiable user, forcing traffic analysis of the
encrypted packets to classify the user's behavior.  An attacker that tries to
classify visited websites is said to perform Website Fingerprinting
(WF)~\cite{cheng1998traffic,DBLP:conf/sp/SunSWRPQ02,Hintz02,DBLP:conf/ccs/LiberatoreL06,HerrmannWF09,PanchenkoNZE11}.
Many questions about the practicality of WF attacks have been raised, ranging
from how to keep a trained dataset updated to managing false
positives~\cite{JuarezAADG14,perryCrit,realistic,onlinewf}. False positives in
WF may be ruled out using side-channels~\cite{JuarezAADG14,wfwo}.  For example,
an attacker with access to (traffic to~\cite{SibyJDVT20}) Google's public DNS
resolver can use it to confirm if a website visit really happened over
Tor~\cite{GreschbachPRWF17}.

Side-channels that leak information about exiting traffic are in fact
many~\cite{wfwo}.  For example, during the course of a website visit there may
be interactions with DNS resolvers, OCSP responders, real-time bidding
platforms, and CDNs.  An attacker that is able to query or gain access to the
resulting datasets learns partial information about destination traffic, notably
without ever observing any of the exiting TCP flows typically associated with
correlation attacks on Tor~\cite{JohnsonWJSS13,deepcorr}.  Depending on the ease
of accessibility (e.g., does it require Google reach), reliability (e.g., are
there any false positives), and coverage (e.g., is it only applicable for a
small fraction of exit traffic), the impact of a given side-channel will be more
or less urgent to address with mitigations and/or defenses~\cite{tor}.

\subsection{Timeless Timing Attacks in Tor's DNS}
Timing attacks exploit that an operation takes more or less time to execute
depending on something secret.  The attacker's goal is to infer the secret
information by merely observing the non-constant execution times, e.g., to
recover a private key~\cite{timing-attack}, decrypt a ciphertext~\cite{lucky13},
or check if a domain is cached by a Tor exit~\cite{wfwo}.  A remote timing
attack takes place over a network.  Repeated measurements and statistics are
usually required to account for network jitter, which adds noise to the observed
timings~\cite{remote-timing-attacks}.  Van Goethem~\emph{et~al.}~\cite{timeless}
proposed a technique that eliminates all network jitter in remote attacks.  It is applicable if
two requests can be sent to arrive at the same time, request processing is
concurrent, and the order in which responses are returned reflects differences in
execution time.

We find that Tor's DNS cache at exits fulfills all three criteria of a timeless
timing attack, allowing anyone to determine if a domain is cached or not by
sending a single TLS record.  The attack is reliable (neither false positives
nor negatives), confirmed by using our prototype to make 12M network
measurements against our own domains.  The attack is also repeatable, making the
exact time that a domain was inserted into the cache inferable due to
determinism in Tor's TTL logic.

Figure~\ref{fig:setting} provides a summary of how the ability to infer whether
domains are (un)cached at exits make WF attacks more practical.
The attacker observes encrypted traffic from a client
to a guard relay at time $t$, classifying the network trace as associated with
\texttt{foo.org}.  The attacker then conducts timeless timing attacks against
all exits in the Tor network to determine if \texttt{foo.org} was really visited
by \emph{someone} at time $t$.  If the answer is yes the classification is
accepted, otherwise it is rejected.  Prior work by Pulls and Dahlberg show that
the capability to determine whether a website was visited from Tor at time $t$
removes virtually all false positives in WF attacks for all but the most popular
websites on the web~\cite{wfwo}.  We provide further evidence that this is a
realistic capability to assume by demonstrating that \emph{any attacker with an
Internet connection could have used it in attacks for the last two decades}.
While it is a powerful capability to eliminate false positives, the overall
success in linking users with their website visits also depends on the WF
attack~\cite{JuarezAADG14,perryCrit,realistic,onlinewf}.

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{img/setting}
	\caption{%
		WF with an attacker that rules out false positives by checking
		that the expected DNS records were cached at the right time by
		conducting timeless timing attacks against exits.
	}
	\label{fig:setting}
\end{figure}

\subsection{Preload Defenses and Measurements}
Patching Tor's DNS cache to resist (timeless) timing attacks is challenging
without hurting performance.  For example, making all DNS lookups constant time
would defeat the purpose of having a cache.  The idea of our long-term defense
is to remove harmful cross-circuit caching that is unlikely to boost performance
while still safely caching useful domains.  The Tor-network measurements of Mani
\emph{et~al.}~\cite{ManiWJJS18} tell us that web-traffic from the Tor network
matches that of the rest of the Internet, following popularity lists like
Alexa~\cite{alexa}. What should boost cross-circuit performance is the upper
parts of a representative popularity list; not the long tail of infrequently
visited sites. This is the intuition of our defense.  Preload a list of popular
domains that are cached and continuously refreshed by all exits.  A domain name
is either always cached as part of the preload list or not shared across
circuits at all.

We conduct four months of measurements in the live Tor network to evaluate 400
popularity lists derived from Alexa~\cite{alexa}, Cisco
Umbrella~\cite{umbrella}, and Tranco~\cite{tranco}.  To put our results into
perspective, we also measure a baseline of Tor's current DNS cache performance.
The measurement method is to collect aggregated counters every 15 minutes, e.g.,
the number of lookups cache-hits, and memory overhead, from two 100 Mbit/s
relays with web and permissive exit port policies.

Tor's mean \emph{cross-circuit} cache-hit ratio is currently 11\% (web) and 17\%
(permissive).  Variants of Alexa/Tranco top-200 (web) and Alexa/Tranco top-700
(permissive) achieve the same cross-circuit cache-hit ratios.  A preload list
from the top-10k can achieve 2--3 times higher cross-circuit cache-hit ratios at
the cost of at most 60 MiB memory and some increased resolver load (manageable
in part due to RFC 8767~\cite{rfc8767}).  Throughout the entire measurement we
noted only a slight decline in effectiveness while using stale preload lists
(i.e., when using four-month-old lists at the end). This adds to the feasibility
of using preload lists, as in practice someone has to assemble and deliver them
to all exits in the Tor network.

\subsection{Contributions and Outline}
Our contributions are as follows:

\begin{itemize}
	\item Performance measurements of the DNS cache in Tor over four months from
	two exits, showing an average 80--83\% cache-hit ratio with approximately
	10,000 entries in the cache; around 11--17\% of the observed cache hits are
	due to the cache being shared across circuits, and the number of lookups
	appears weakly correlated with exit probability
	(Section~\ref{sec:torcache}).
	\item Demonstration of a timeless timing attack that probes for cached
		domains in Tor's DNS cache without any false positives or false
		negatives after 12M repetitions against our own domain in the Tor
		network (Section~\ref{sec:attack}).
	\item Mitigations based on fuzzy TTLs and cover lookups that add some
		limited short-term protections (Section~\ref{sec:short}).
	\item A long-term redesign of Tor's DNS cache that defends against
		(timeless) timing attacks.  Cache-hit ratios can be tuned to offer
		comparable performance under similar resource usage as today or to
		significantly improve shared cache-hit ratios (2--3x) with a modest
		increase in memory usage and resolver load, notably invariant to
		exit probability as preload lists are fixed (Section~\ref{sec:long}).
\end{itemize}

Section~\ref{sec:background} provides necessary background on DNS and Tor,
Section~\ref{sec:related} discusses related work, and
Section~\ref{sec:conclusion} offers conclusions, followed by the availability of
our research artifacts.

We would like to highlight that Sections~\ref{sec:torcache:ethics},
\ref{sec:attack:ethical}, and \ref{sec:long:preload:ethics} describe ethical and
safety precautions to ensure that no users were harmed by our research and to
maximize its positive impact. We responsibly disclosed our timeless timing
attack to the Tor Project and engaged with the Tor Research Safety Board as well
as our university's ethical review process as part of performing network
measurements to inform our defenses.
