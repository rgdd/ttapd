\section{Timeless Timing Attack} \label{sec:attack}

Past work demonstrated timing attacks against Tor's DNS cache~\cite{wfwo}. In
short, anyone can observe the latency of a domain lookup to determine if it is
more or less likely that an answer is (not) cached.  A quick response is more
likely to be cached, thereby leaking information about past traffic on an exit.
A downside of such a remote timing attack is that it is subject to network
jitter while traversing hops in the Tor network.  We show how to bypass this
limitation by constructing a timeless timing attack that is immune to network
jitter~\cite{timeless}.  Notably the attack does not require any special
capabilities, just Internet access and a very modest computer.

Section~\ref{sec:attack:detailed} outlines the attack, followed by a description
of our prototype implementation in Section~\ref{sec:attack:prototype},
evaluation in Section~\ref{sec:attack:measurements}, as well as ethical
considerations in Section~\ref{sec:attack:ethical}.

\subsection{Detailed Description} \label{sec:attack:detailed}
An exit's processing of an incoming RESOLVE cell depends on if an answer is
cached or not, see Figure~\ref{fig:resolve}.  An answer may already be available
and a RESOLVED cell can be scheduled for sending immediately (``cached'').
Otherwise an answer is not yet available and a resolve process needs to take
place concurrently to avoid blocking (``uncached''). We construct a timeless
timing attack by exploiting the fact that scheduling RESOLVED cells for sending
with different concurrent timings depend on if an answer is cached (send
immediately) or uncached (send based on an event later on)~\cite{ctor-1}.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.9\columnwidth]{img/resolve}
	\caption{%
		Processing of an incoming RESOLVE cell at an exit relay.
		Answers of concurrent resolves are triggered by events.
	}
	\label{fig:resolve}
\end{figure}

\subsubsection{Attack Outline}

Suppose that we craft two RESOLVE cells for \texttt{example.com} and
\texttt{evil.com} such that they are processed by an exit \emph{directly after
each other without any events in between}.  Further suppose that
\texttt{evil.com} is cached.  The first RESOLVE cell is \texttt{example.com}.
The second RESOLVE cell is \texttt{evil.com}.  Following from the flow in
Figure~\ref{fig:resolve}, we can determine if \texttt{example.com} is (un)cached
by observing only the order in which the two RESOLVED cells come back.  The
order will be switched if \texttt{example.com} needs concurrent resolving
because \emph{the answer is not available until after an event} (uncached).
Otherwise the order is preserved (cached).  Sending two requests to be processed
at the same time and exploiting concurrency as well as differences in processing
time that affects the response order is what makes it
\emph{timeless}~\cite{timeless}.

Figure~\ref{fig:timeless} provides a detailed description on how to satisfy the
presumed setup.  The attacker starts by looking up its own domain name for a
selected exit.  This ensures that \texttt{evil.com} is cached. Next, two RESOLVE
cells are sent in the same TLS record from a hop proceeding the exit. Both cells
will be unpacked at the same time by TLS~\cite{ctor-2},
and when processing starts all available cells will be handled before giving
control back to Tor's main loop~\cite{ctor-3}.
Now recall that Tor is single-threaded.  An event from any concurrent DNS
resolve can thus not be completed before all unpacked cells were fully
processed.  This ensures that the order in which our two RESOLVED cells come
back in is guaranteed to leak if \texttt{example.com} is (un)cached as long as
both RESOLVE cells arrived together in-order and \texttt{evil.com} is really
cached.

\begin{figure*}
	\subfloat[][uncached]{%
		\includegraphics[width=\columnwidth]{img/uncached}
		\label{fig:timeless:a}
	}
	\subfloat[][cached]{%
		\includegraphics[width=\columnwidth]{img/cached}
		\label{fig:timeless:b}
	}
	\caption{%
		The attacker ensures a domain \texttt{evil.com} is cached. Next, two
		RESOLVE cells are sent to arrive at the same time in-order.  The relay
		processes both cells before triggering any resolve event.  This means
		that answers can only be sent directly if no resolving is needed.  The
		order of RESOLVED cells switch if \texttt{example.com} is uncached.
		Otherwise the order is preserved. }
	\label{fig:timeless}
\end{figure*}

It should be noted that an attacker can gain full control of how their TLS
records are packed to exits by either running a modified Tor relay or creating
one-hop circuits.  In practise, it is also possible to omit the step of caching
\texttt{evil.com} and instead send a \texttt{RESOLVE} cell containing an IP
address.  Tor will simply echo the IP as if it was cached~\cite{ctor-4}.  We
describe the attack without this optimization because it is more general.

\subsubsection{Repeated Attack to Learn Insertion Time}
So far we described how to determine if a domain is (un)cached at an exit.
Figure~\ref{fig:attack-repeated} shows how to derive the exact time that a
domain was added to an exit's DNS cache.  First determine whether the domain's
TTL will be clipped to 300 or 3,600 seconds by observing the TTL returned from
the authoritative name server or the configured resolvers of the
exit~\cite{GreschbachPRWF17}.  Then repeat the timeless timing attack
periodically until the domain is no longer cached, say, once per second. Suppose
the expected clip is 300 seconds and the attack started at time $t$.  If it
takes $x < 300$ seconds for the entry to become uncached, it was added to the
exit's DNS cache at time $t+x - 300\textrm{s}$.  Observing $x > 300$ seconds
means that a different party inserted the entry into the cache between probes
(may happen for some of the most frequently looked-up domains, depending on
probing frequency).  To recover, the attacker can perform the same steps again
until they succeed.  For example, with two tries the initial insertion happened
at $t+x - 600\textrm{s}$. Notably these estimates cannot be more precise than
the attacker's repetition interval.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.75\columnwidth]{img/repeat-attack}
	\caption{%
		Repeated timeless timing attack to infer the exact time that a domain
		was cached by someone at an exit relay.  For example, if the expected
		clip is 300s ($\mathsf{ttl}\le300$s), the attack is repeated every
		second, and the observed $x$ is 40s, then caching of
		\texttt{example.com} happened at time $\approx t-260$s. }
	\label{fig:attack-repeated}
\end{figure}

\subsubsection{Discussion}
While an attacker can determine if a domain is cached by an exit and if so the
exact time it was added, the attacker cannot determine the number or timings of
lookups for a domain after entering the cache.  In isolation, the attacker also
cannot determine which identifiable user cached a given domain.

It is easy to conduct the attack in parallel because
probing for the status of \texttt{foo.org} is completely independent from
\texttt{bar.org} at the same relay as well as other probes on different relays.
In other words, an attacker can probe a single domain on all exits
simultaneously, many different domains at a single exit, or both.  Network-wide
probes for the same domain may be detectable by observing the DNS caches of
multiple relays and correlating their contents. However, note that a risk-averse
attacker~\cite{AumannL07} may spread their probes over time (five or sixty
minutes) and domains (expected twelve domains per website on Alexa top-1M
websites~\cite{GreschbachPRWF17}), if the goal is to confirm a website visit.

An example use-case for a parallel attack is answering network-wide queries, for
example, ``is \texttt{foo.org} visited more frequently than \texttt{bar.org}, or
did any Tor user visit \texttt{baz.org} at a particular point in time?'' The
latter is an instantiation of a so-called website oracle~\cite{wfwo}.  Website
oracles remove virtually all false positives in WF attacks for all but the most
popular websites on the web, and WF attacks may connect identifiable users with
visited websites.  See Figure~\ref{fig:setting} in
Section~\ref{sec:introduction} for an overview of this attack setting.

\subsection{Prototype Implementation} \label{sec:attack:prototype}

We prototyped our timeless timing attack so that it runs for a given exit and a
list of domains.  Figure~\ref{fig:attack} shows the overall setup which consists
of \texttt{carml},
\texttt{tor-resolve}, a locally patched Tor process, and a Python script
automating the entire setup. First Tor is started, a \emph{one-hop circuit} is
built to the selected exit, and all streams are attached to it using
\texttt{carml}.  Next, \texttt{tor-resolve} is used to send a special lookup
query for \texttt{example.com} by simply appending a magic string
\texttt{---sc}.  The patched Tor process splits such requests into two RESOLVE
cells in the same TLS record: one for the specified domain, and another one that
is guaranteed to not need any concurrent resolving. Finally Tor sets the output
to \texttt{0.0.0.0} if the resulting RESOLVED cells switched order, otherwise
\texttt{1.1.1.1} (arbitrary constants).
After processing all domains Tor is closed and the output
is a list where each item is zero (uncached), one (cached), or negative
(unknown, e.g., due to a resolve timeout, a stream attach failure, or a vanished
circuit).
The complete attack required less than 100 lines of C to patch Tor, as well as
200 lines of Python to make it fully automated.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.7\columnwidth]{img/attack}
	\caption{%
		Local attack setup consisting of \texttt{carml} to build one-hop
		circuits, \texttt{tor-resolve} to inject queries, and a patched
		tor process that transforms them into timeless timing attacks.
	}
	\label{fig:attack}
\end{figure}

\subsection{Network Measurements} \label{sec:attack:measurements}

We conducted measurements in the live Tor network to evaluate the reliability of
our prototype with four parallel instances of
the setup in Figure~\ref{fig:attack} on a system with an Intel(R) Xeon(R) CPU
E5-2630 @ 2.30GHz and 4GB of DRAM. All targeted domains were our own, see
ethical considerations in Section~\ref{sec:attack:ethical}. In total there were
$14,446$ runs between May 17--26, 2022.  Each run used an exit that was sampled
uniformly at random.  Assuming $1,000$ exits at all times (conservative), the
individual select probability should not exceed $0.004$ per run.  Each run
performed up to $1,000$ timeless timing attacks, chunked into $500$ attacks per
circuit and alternating between uncached and cached lookups by specifying a
unique domain twice in a row: \texttt{<counter>.<timestamp>.<instance
id>.example.com}. The maximum runtime was set to ten minutes.  Each query also
had a ten second timeout.  In the advent of errors like circuit failure or
timeouts, the remainder of the run was canceled but all results up until that
point were collected.  The average number of DNS requests leaving the Tor
network from \emph{all four combined instances} was $8.6$ per second.  The
effective queries per second was slightly higher due to brief pauses while
setting up a new run.  For reference, Sonntag reported in 2018 that the DNS
resolver of an exit with $200$Mbit/s received an average and maximum of $18$ and
$81$ requests per second~\cite{sonntag-metrics}. Earlier,
Figure~\ref{fig:lookups} also showed significant variability in lookups.
Handling our per-exit overhead during a couple of minutes should thus be
insignificant when compared to regular patterns for DNS traffic in the network.

Table~\ref{tab:attack} summarizes our results.  After 12M timeless timing
attacks, there were no cases of uncached lookups being reported as cached and
vice versa.  This is consistent with the description in
Section~\ref{sec:attack:detailed}: neither false positives nor false negatives
are expected.  The observed probability to not get an answer due to
detectable failures were $0.00025$.

\begin{table}[!ht]
	\caption{%
		Timeless timing attack results.  Neither false negatives nor
		false positives were observed with 6M repetitions each.
	}
	\begin{tabular}{c|ccc}
		Type       & Got uncached & Got cached  & Failures \\
		\hline
		Uncached   & $6,034,779$  & $0$         & $2,858$  \\
		Cached     & $0$          & $6,034,594$ & $142$    \\
	\end{tabular}
	\label{tab:attack}
\end{table}

\subsection{Ethical Considerations} \label{sec:attack:ethical}

We responsibly disclosed our attack to the Tor Project through their security
list. The submitted disclosure included a theoretical attack description, a
prototype implementation with measurements showing how reliable it was, as well
as a sketch of short-term and long-term defenses. As part of our dialog, we also
coordinated with the Tor Project on submitting this paper to USENIX Security to
get peer review.

The conducted network measurements targeted domains under our own control. This
ensured that we did not learn anything about real Tor users.  Performance
overhead on exits and the Tor network at large was also modest, see
Section~\ref{sec:attack:measurements}.  In other words, the downsides were
negligible while the significance of evaluating \emph{real-world reliability}
was helpful to inform and motivate the need for mitigations and defenses.
