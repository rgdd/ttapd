\section{Background} \label{sec:background}
The remainder of the paper requires preliminaries about DNS
(Section~\ref{sec:background:dns}), in particular in relation to Tor
(Section~\ref{sec:background:tor}).

\subsection{DNS} \label{sec:background:dns} DNS is a hierarchical system that
maps domain names (``domains'') to IP addresses.  The hierarchy is composed of
root servers, top-level domain (TLD) servers, and authoritative name servers.
Root servers are aware of TLD servers like \texttt{.com}.  TLD servers are aware
of authoritative name servers in their zone like \texttt{example.com}.
Authoritative name servers are aware of the actual answers to a domain lookup.
A domain lookup for \texttt{example.com} involves asking the root server for the
TLD server of \texttt{.com}; the TLD server for the authoritative name server of
\texttt{example.com}; and finally the authoritative name server for the IP
address of \texttt{example.com}.  The resolve process is typically performed
iteratively in plaintext over UDP by a third-party resolver that caches
responses, e.g., to improve performance.  The default is usually to rely on ISP
DNS resolvers.  It is also possible to configure other ones, e.g., Google's
\texttt{8.8.8.8} or self-hosted using \texttt{unbound}, \texttt{bind}, etc.

Of note is that the resolved domains are associated with a Time To Live (TTL)
value.  As the name suggest, it is the amount of time that a resolved domain
should be considered fresh.  TTL values are sometimes overridden in caches to
improve reliability~\cite{rfc8767,MouraHMSD18} or preserve
privacy~\cite{GreschbachPRWF17}.

\subsection{Tor} \label{sec:background:tor}
The Tor network is composed of thousands of relays that route encrypted traffic
on behalf of millions of daily users~\cite{tor,ManiWJJS18}.  Ordinary uses of
Tor include preserving privacy, safety and freedom as well as facilitating
dissent and circumventing censorship~\cite{tpo-who-uses-tor,tpo-russia}.  Access
to the Tor network is easy using Tor Browser (TB), which is configured to proxy
all traffic through a local Tor process that takes care of routing.  TB adds
many other protections that are orthogonal to our work~\cite{tb}.

During a regular website visit a circuit is built through a guard, middle, and
exit relay. The first relay is fixed in a small guard set that rarely changes
once selected, while the middle and exit relays are randomly selected weighted
by bandwidth for each new circuit.  A circuit may have many streams (analogous
to TCP/IP connections), typically corresponding to separate flows for a
particular destination.  Control traffic and data is transported through the
network in fixed-size cells that are encrypted in layers.  At each hop in a
circuit, one layer of encryption is peeled-off. Outstanding cells from relay A
to relay B are sent in a shared channel that is TLS protected.  Public keys,
relay identities, and more are discovered in Tor's consensus, which is secure if
a threshold of trusted directory authorities act honestly.

We are particularly interested in how Tor interacts with DNS.  To look up a
domain, the user's Tor process may send a RESOLVE cell that requests resolution
by the exit.  Some exits are configured with their own iterative resolvers,
while others rely on DNS from their ISP or other
third-parties~\cite{GreschbachPRWF17}.  The answer to a lookup is stored in the
exit's cache, but with the TTL \emph{clipped} to 300 or 3600 seconds depending
on if the TTL is $\le 300$ seconds or not.  A RESOLVED cell is then sent to the
user, who only gets to see the clipped TTL regardless of how long it has been
stored in the cache to avoid leaking information about past exit traffic (like
the insertion time which would be trivial to infer from a counted-down TTL).  If
too many entries are added to Tor's DNS cache and memory becomes a scarce
resource, an Out-Of-Memory (OOM) job deletes domains until freeing enough
memory.  This is all controlled by an event-driven single-threaded main loop.

Of further note is that TB is based on Firefox.  As part of connecting to a
website, DNS is handled transparently through a SOCKS proxy provided by the
local Tor process. Requests to connect to a domain through the SOCKS proxy
results in the user's Tor process sending a BEGIN cell to establish a connection
to the destination, which in turn triggers domain resolution at the exit.  In
other words, there are two ways to look up domains: RESOLVE cells and BEGIN
cells.  At no point is any resolved IP address cached in TB or in the user's Tor
process.  This prevents shared state (the cache) from being used to
fingerprint a user's activity across different circuits.

We continue our introduction to Tor's DNS cache next while describing the
first measurement of its performance.
