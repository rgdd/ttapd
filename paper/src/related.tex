\section{Related Work} \label{sec:related}

Van Goethem \emph{et~al.}~\cite{timeless} originally proposed timeless timing
attacks, showing significant improvements against HTTP/2 web servers, Tor onion
services, and EAP-pwd.  All timeless timing attacks exploit concurrent
processing, e.g., in HTTP/2, by filling buffers at the relay closest to an onion
service, or packing two authentication requests in EAP-pwd into the same RadSec
(TLS over TCP) packet. The latter was the inspiration for our timeless timing
attack on Tor's DNS cache, i.e., packing two RESOLVE cells into a single TLS
record.

There has been a long body of work on how to safely perform measurements of the
Tor network~\cite{privcount,privex,histor,FenskeMJS17}, laying the foundation
for safely performing large-scale measurements~\cite{ManiWJJS18,JansenTH18}. Our
timeless timing attack enables anyone to do network-wide measurements for exact
domains on specific exits with a precision of at least one second.  This is
highly invasive and a useful resource to deanonymize Tor-users, discussed
further shortly. Our network measurements to inform the design of defenses have
been focused around the DNS in Tor.  Similar to other related work (see below),
we focused on how to make those limited measurements safe; not on how to broadly
perform a much wider range of measurements safely.

Greschbach \emph{et~al.}~\cite{GreschbachPRWF17} investigated the effect of DNS on
Tor's anonymity. They quantified the use of DNS resolvers in the network, the
impact of choice of resolver on correlation and confirmation attacks, and how to
incorporate observed DNS traffic with website fingerprinting
attacks~\cite{cheng1998traffic,DBLP:conf/sp/SunSWRPQ02,Hintz02,DBLP:conf/ccs/LiberatoreL06,HerrmannWF09,PanchenkoNZE11}
to make improved correlation attacks. In their construction, DNS traffic is used
to either reduce the number of websites to consider during classification or to
confirm classification. A key observation was that Tor, due to a bug, clipped
all TTLs to 60 seconds. This was resolved and lead to the current approach of
clipping to 300 or 3,600 seconds.  One of our short-time mitigations update
these clips to be fuzzy.

Greschbach \emph{et~al.}~\cite{GreschbachPRWF17} also measured DNS requests from
an exit for both web and a more permissive exit policy in 2016. The collection
was done by observing DNS requests to the exit's resolver and aggregating
results into five-minute buckets. Similarly, we aggregate over time in 15-minute
buckets and do not directly collect resolved domains. They found a small
difference between exit policies, with the permissive exit having slightly fewer
(3\% smaller median) lookups. Our results are very different: the permissive
exit policy resulted in significantly more (double the median) lookups.

Pulls and Dahlberg~\cite{wfwo} generalized the classifier confirmation attack of
Greschbach \emph{et~al.}~\cite{GreschbachPRWF17} into a new security notion for website
fingerprinting attacks, and further explored the use of DNS. They showed that
timing attacks were possible in Tor's DNS cache, performing network-wide
measurements on a domain under their control with a true positive
rate of 17.3\% when attempting to minimize false positives. We use a similar
method for measurements, but our attack is significantly better with a
100\% true positive rate and no false positives at all.

Sonntag collected hourly data from the resolver of an exit during five months in
2018~\cite{Sonntag19,sonntag-metrics}. In terms of frequency, they noted about
18.5 requests per second, with a peak of 291,472 requests in an hour. The
average is higher than ours (3.9 and 7.8 requests per second) while the peak
significantly smaller (1,183,275 requests in 15 minutes). Sonntag also analyzed
the actual domains looked up, including categorization (porn, social network,
shopping, advertisement etc). We do not collect domains; only cache-hits as part
of popularity lists by aggregating domains into buckets like top-100, top-1k,
etc.

Mani \emph{et~al.}~\cite{ManiWJJS18} used PrivCount~\cite{privcount} and
PSC~\cite{FenskeMJS17} to safely make extensive network-wide measurements of the
Tor network. They measured, e.g., circuits, streams, destination ports, and exit
domains at exits, as well as client connections, churn, composition, and
diversity at clients. Their exit probability ranged between 1.5--2.2\%, compared
to our peak of 0.1\%. While our data is much more limited and targeted around
DNS, there are two interesting comparisons to consider:
\begin{itemize}
    \item Mani \emph{et~al.} observed 2.1 billion exit streams inferred in the
    network every 24 hours. Extrapolating on our lookup statistics we have an
    average of 6.3 billion lookups, which corresponds to the number of
    streams\footnote{Streams either do a lookup with RELAY\_BEGIN or are closed
    after a RELAY\_RESOLVE cell. Timeout and retries are possible on resolver
    failure, but the way we measure hides those extra lookups.}. This suggests a
    significant increase ($\approx3x$) in the number of streams in the Tor network since 2018.
    \item Mani \emph{et~al.} measured the frequency of how well the primary
    domain on a circuit matched the Alexa top-one-million list. We transform
    their reported relative counts and compare it to the relative count of
    average lookups in the same intervals in our dataset for top-10k, shown in
    Figure~\ref{fig:mani:popularity}. Note that this only uses data from phase
    one of our data collection. Broadly, we see that their results show
    significantly more traffic to top-10 than any of the lists we use. That
    said, our data supports one of Mani \emph{et~al.}'s conclusion that the
    popularity lists are reasonably accurate representations of traffic from the
    Tor network.
\end{itemize}

\begin{figure*}
    \subfloat[][web]{%
        \includegraphics[width=\columnwidth]{img/plot_popularity_match-web.pdf}
        \label{fig:mani:popularity:web}
    }
    \subfloat[][permissive]{%
        \includegraphics[width=\columnwidth]{img/plot_popularity_match-permissive.pdf}
        \label{fig:mani:popularity:perm}
    }
    \caption{%
    Comparison of \emph{relative popularity} of popularity rankings with the
    results of Mani \emph{et~al.}~\cite{ManiWJJS18}.}
    \label{fig:mani:popularity}
\end{figure*}

The relatively recent RFC 8767 \cite{rfc8767} allows for DNS data to be served
``stale'', i.e., after expiry according to its TTL, in the exceptional
circumstance that a recursive resolver is unable to refresh the information. In
case data goes stale, RFC 8767 suggests serving it for at most one to three days.
The background of RFC 8767 aptly motivates this with the saying that
\emph{``stale bread is better than no bread''}. In addition to serving
potentially stale data, modern resolvers like Unbound~\cite{unbound} further
support prefetching: preemptively refreshing domains in the cache before TTL
expiry. These measures all serve to improved reliability and have been found to
be used for sake of resiliency \cite{MouraHMSD18}. Tor already clips TTLs, in a
sense serving stale data for the vast majority of domains. Our preload design
takes this further by introducing continuous prefetching of domains on a fixed
allowlist.

Two decades ago, Jung \emph{et~al.}~\cite{jung} found that cache-hit ratios on
the order of 80--87\% are achieved if a resolver has ten or more clients and
TTLs are at least ten minutes.  More recently Hao and Wang~\cite{hao-and-wang}
reported that 100k cached entries are required to achieve a baseline of 86\%
cache-hits for a first-come first-serve cache in a university network.  Their
dataset had similar characteristics to a DNS trace collected for an ISP resolver
by Chen \emph{et~al.}~\cite{chen} with regards to \emph{disposable domains} that
are never requested more than once in the long-tail of DNS; out of the 11\% of
domains that are not disposable, 5\% and 30\% of them have cache-hit ratios of
at least 95\% and 80\% respectively.  It appears that fewer disposable domains
are resolved in Tor because the observed cache sizes are not large enough for
89\% unique lookups.  Achieving an 80\% cache-hit ratio with a cache of 10k
entries does not seem to be an outlier.
