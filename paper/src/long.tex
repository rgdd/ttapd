\section{Redesigning Tor's DNS Cache} \label{sec:long} 
To address (timeless) timing attacks in Tor's DNS cache we considered a
number of possible smaller changes.  All of them failed for different reasons,
however.  Section~\ref{sec:long:strawman} presents a straw-man design that is
helpful to understand \emph{why}, while at the same time being closely related
to the properties achieved by the preload DNS cache design in
Section~\ref{sec:long:preload}.  Section~\ref{sec:long:evaluation} presents an
extensive evaluation that answers questions about how feasible and performant
our proposal is.

\subsection{Straw-man Design} \label{sec:long:strawman}
We omit all but one
straw-man design that is particularly important to understand the proposed
redesign in Section~\ref{sec:long:preload}: \emph{simply remove Tor's DNS
cache}.  If there is no DNS cache to speak of in Tor, it is easy to see that
there cannot be any (timeless) timing attacks against Tor's DNS cache (because
it does not exist).  What these attacks would instead target is the exit's DNS
resolver which also has a cache.  At a first glance it may seem like an
insignificant improvement that just moves the problem elsewhere. This would be
the case if every exit used its own dedicated DNS resolver. However, an exit may
share a resolver with other exits or most importantly clients outside of the Tor
network.  A prime example is the resolver of the ISP of the exit.  Any inference
made from the state of shared resolvers would thus not be directly attributable
to activity on the Tor network. This would therefore make \emph{false positives}
a reality with regards to if a domain was cached or not as a consequence of
activity in the Tor network.

Introducing false positives to the timeless timing attack itself is in general
challenging because an answer needs to be available at the same time regardless
of there being a cache hit or miss. False negatives may seem easier and could
make the attacker discard otherwise correct classifications, e.g., because an
attack only works half of the time. However, without false positives, attackers
are still able to reliably remove otherwise incorrect classification through
confirmation~\cite{wfwo}. Because websites typically make use of multiple domain
names, defenses that add random delays to responses (to cause false negatives)
would need to consistently add similar delays for all relevant domains tied to
websites or other user activity the attacker is trying to infer. The semantics
surrounding user activity is hard if not impossible to capture at the DNS level.
Therefore, all false negative defenses we could think of failed.

Now suppose that Tor has no DNS cache and exits always use a shared resolver
that may introduce false positives.
A major downside is that performance would take a significant hit due to the
lack of a cache in Tor, especially since a shared resolver is likely not running
locally, but provided by the ISP or some third-party.  It is likely that both
page-load latency and resolver load would increase. Worsening performance and
especially latency is the opposite of what the Tor project is working
towards~\cite{tor-congestion,tor}.  Next we show how to get the good properties
of not having a DNS cache in Tor (potential for false positives) while improving
performance.

\subsection{The Preload DNS Cache} \label{sec:long:preload} 

This is not only a defense against (timeless) timing attacks in the DNS cache,
but a complete redesign of Tor's DNS cache. Ultimately, \emph{what we want to
achieve is false positives for an attacker trying to determine client activity
in the Tor network with the help of DNS}. The only way to achieve this---upon
learning that a domain associated with some activity has been looked up---is if
there is a possibility that this domain lookup was caused from outside of the
Tor network. Therefore, as a starting point, we assume that the Tor Project
would strongly encourage exit operators to not run local resolvers dedicated to
exits. Instead, exit operators should configure their systems to use their ISP
resolvers or use a third-party provider. Greschbach
\emph{et~al.}~\cite{GreschbachPRWF17} investigated the effect of DNS on Tor's
anonymity, including resolver configuration, and found that using the ISP's
resolver would be preferable.

First remove all of Tor's current DNS caching as in our straw-man design.  The
preloaded DNS cache instead contains two types of caches: a same-circuit cache
and a shared preload cache, see Figure~\ref{fig:preload}.  The preloaded cache
only contains domains from an allowlist.  This allowlist is compiled by a
central party (e.g., by the Network Health team in the Tor Project) by visiting
popular sites from several different vantage points. The allowed domains are
then delivered to exits and continuously resolved to IPs by each exit. During
domain resolution on a circuit, the client's lookup first hits the preload
cache.  If the domain is preloaded, a cache hit is guaranteed regardless of if
anyone performed a lookup before. Therefore, it is safe to share this cache
across circuits without leaking information about past exit traffic.  On a cache
miss, the circuit's same-circuit cache is consulted.  As the name suggests, this
cache is shared for streams on the same circuit but not across different
circuits.  Due to Tor's circuit isolation, an attacker is unable to probe any
other cache than their own.  Therefore, (timeless) timing attacks are eliminated
(similar to if Tor did not have a DNS cache at all), but without removing the
possibility of cache hits.

\begin{figure*}
	\centering
	\includegraphics[width=\textwidth]{img/preload}
	\caption{%
		Overview of the preloaded DNS cache design.  A central party
		visits sites on a popularity list from different vantage points
		to compile an allowlist of domains that each relay keeps
		preloaded at all times by resolving them continuously.  DNS
		looks-ups start in the shared preload cache and moves on to a
		dynamic cache that is never shared across circuits on cache
		misses.
	}
	\label{fig:preload}
\end{figure*}

Including a same-circuit cache in the defense is motivated by Tor's significant
same-circuit caching to retain performance, see
Figures~\ref{fig:metrics:hitsweb} and~\ref{fig:metrics:hitsperm} in
Section~\ref{sec:torcache:metrics}. One can confirm that this is most likely due
to Tor Browser opening several concurrent connections by referring to the
\texttt{network.http.max-persistent-connections-per-pro xy} option and/or
enabling debug logging\footnote{%
	\url{https://gitlab.torproject.org/tpo/applications/tor-browser/-/wikis/Hacking\#debugging-the-tor-browser}
}, observing that multiple streams are often created to the same destination.
Note that these destinations are domains and not IPs, and that neither TB nor
the client-side Tor process has any notion of a DNS cache to prevent
cross-circuit fingerprinting (see Section~\ref{sec:background:tor}).  While a
hypothetical \emph{per-circuit client-side cache} would be an option, it would
per definition not be able to generate cache hits for concurrent resolves
(without violating circuit isolation, potentially leading to cross-circuit
fingerprinting) and put pressure on exits unless they do the appropriate
caching.  This is why our design places the same-circuit cache at exits instead
of clients.

A preload cache is also motivated by performance, however without any of the
harmful cross-circuit sharing.  The remainder of this section explores the
performance impact of compiling an allowlist from popularity lists---like
Alexa~\cite{alexa}, Cisco Umbrella~\cite{umbrella}, and
Tranco~\cite{tranco}---by comparing the resulting cache-hit ratios to baseline
Tor today.  The preloaded DNS cache is inspired by RFC 8767~\cite{rfc8767} which
allows resolvers to serve stale data in some cases (see
Section~\ref{sec:related}). Here, exits keep domains on a preloaded allowlist
fresh on a best-effort level, serving stale records if necessary. Upon shutdown,
exits could persist IPs in the preload cache to disk as a starting point on
startup.  Upon startup, if the preload cache have yet to be populated with IPs,
simply treat lookups as cache misses.  We discuss periodic refresh overhead
further in Section~\ref{sec:long:preload:resolverload}.

\subsection{Data Collection} \label{sec:long:preload:collection}

As part of understanding Tor's DNS cache (Section~\ref{sec:torcache}) we also
collected data to be able to evaluate the performance of the preload design. In
particular, we evaluate different popularity lists, the impact on cache-hit
ratio, estimated DNS cache size, and how these change over time.

Central to the preload design is domain popularity lists. We included the Alexa
list~\cite{alexa} because that is what Mani \emph{et~al.} showed to be accurate for
Tor~\cite{ManiWJJS18}, the newer Tranco list because it may be more
accurate~\cite{tranco}, and the Cisco Umbrella list because it also contains
``non-web'' domains~\cite{umbrella}.

In addition to considering the popularity lists, we also created \emph{extended}
lists from Alexa and Tranco by visiting each domain on those lists using the
Chromium browser and recording all requests for additional domains. We repeated
this three times from Europe, twice from the US, and twice from Hong Kong by
using a popular VPN service. Each visit was given a timeout of 20 seconds. No
pruning of the resulting extended lists of domains was done. Much can likely be
done to make these lists of domains significantly more comprehensive (e.g., by
considering popular subpages that might contain domains not on the front-page of
websites) and smaller (e.g., by pruning unique tracking domains: in one of our
biggest lists, \texttt{*.safeframe.googlesyndication.com} makes up 8\% of
domains with unique tracking subdomains with no value for caching).  Another
direction to explore that could result in lists that are smaller and/or more
comprehensive would be to tailor them specifically for relays in certain
regions.  For example, website visits from Europe may be treated differently by
website operators due to regulations like the GDPR. (In other words, there could
be differences with regards to \emph{domains}---not to be confused with IPs that
each relay already resolves locally---that are encountered during website
visits.)

Based on the regular and extended popularity lists, we made several lists from
top-10 up to and including top-10,000 in increments. Further, the weekend before
each of the \emph{first four weeks} of data collection (see
Section~\ref{sec:torcache}), we downloaded fresh popularity lists (Fridays) and
generated new extended lists (Saturdays and Sundays). We generated in total
$4*20*5 = 400$ lists: for the first four weeks, 20 lists each for \{Alexa,
Tranco, Umbrella, extended Alexa, extended Tranco\}.

Our data collection involving the lists took place in three phases. The first
phase consisted of the first four weeks with increasingly more lists, which was
followed by two weeks of analysis of our results and dialog with the Tor
Research Safety Board. This lead us to the third and final phase of data
collection where we excluded the vast majority of lists, focusing only on
getting extended data for about eleven more weeks on the most informative and
useful lists (see Section~\ref{sec:long:evaluation}).

\subsection{Further Ethical Considerations} \label{sec:long:preload:ethics}

We discussed the preload additions as part of our other data collection,
received feedback from the Tor Research Safety Board, and passed our
university's ethical review process. 

Our rationale for why including counters for preload lists is safe was as
follows. We collect counters of aggregate lookups that would have been
cache-hits on each list over 15 minutes. Except for the top-10 lists
(non-extended), all other lists contain in the order of 100--1,000 unique
domains aggregated into a single counter. The main harm associated with the
dataset is if they enable an attacker to \emph{rule out} that a particular
website or Tor-user activity took place at our exits (see following paragraph).
So, little to no zero counters in our data is what we set out to achieve.  As an
additional safety precaution our exits only have a $0.1$\% exit probability,
further making any zero counter less useful.

Let us look more closely at the potential harm. For websites, the results of Mani
\emph{et~al.}~\cite{ManiWJJS18} tell an attacker to expect a power-law-like
distribution of website popularity in the network. As discussed in
Section~\ref{sec:torcache:ethics}, we expect on average about 725 website visits
to each exit per 15 minute period. This is \emph{the prior of an attacker}
wishing to perform confirmation or correlation attacks. Most of the visits
should be to popular websites (per definition) and if the dataset allows an
attacker to rule such visits out it may cause harm because it is useful
information to the attacker~\cite{wfwo}. Because of this, we grouped our lists
into intervals of 100s (for top-?00) and 1000s (for top-?000). We stopped at
top-10k because we estimated little utility of caching domains of even less
popular websites. Further, to illustrate when the type of data we collect can be
harmful, the results of Mani \emph{et~al.}~\cite{ManiWJJS18} and Pulls and
Dahlberg~\cite{wfwo} tell us that at some point the logic becomes flipped in
terms of attacker utility: confirming that it was possible that a visit took
place to a \emph{rarely visited website} is useful. The popularity (i.e.,
network base rate) of websites is central. We set out to only collect data on
the most popular of websites/domains, so for us, the focus is on when the
attacker can rule out website visits or some user activity: an attacker already
expects that popular websites/domains are visited.

We analyzed the 1,330,400 sample counters we collected over the first four weeks
for different popularity lists. We found 33 zero counters. All of them belonged
to Alexa top-10 lists from different weeks! Besides Alexa top-10, the next list
with the lowest counter was Tranco top-100 from 20 May with 39 hits. Finding
empty counters for Alexa top-10 was at first very surprising, because the list
contains the most popular websites on the web (e.g., from 20 May:
\texttt{google.com}, \texttt{youtube.com}, \texttt{baidu.com},
\texttt{facebook.com}, \texttt{instagram.com}, \texttt{bilibili.com},
\texttt{qq.com}, \texttt{wikipedia.org}, \texttt{amazon.com}, and
\texttt{yahoo.com}). However, note how the actual \emph{domains} on the list (of
\emph{websites}) do not contain the www prefix nor any other popular subdomain
associated with the sites. This highlights how poor the regular non-extended
lists are at capturing actual \emph{website} traffic. We can further see this
for both Alexa and Tranco in Figure~\ref{fig:preload:heatmap}, presented next in
Section~\ref{sec:long:preload:lists}. Even the top-10k lists have very low
cache-hit ratios.

By comparing a list with a more popular list (which should be a strict subset)
and observing the same counter value it is also possible to infer that
\emph{likely} no visits took place to the unique domains on the less popular
list. (This could happen by chance though.) We found 16,055 (1.2\%) such
samples: 5,073 to top-10k lists, 3,703 to top-[1k,10k) lists, and 7,279 to
top-[200,1k) lists. None of them were to top-100 lists. This might seem alarming
at first glance, but taking a closer look at the lists we find that only 135 of
the samples were to extended lists (77 to x-Tranco top-10k, the highest rank
list was x-Tranco top-600 with one sample). Further, only five of the samples
belonged to a list from Umbrella. The remaining 15,915 samples were to the
regular (non-extended) Alexa and Tranco lists. This is of limited use to
attackers for popular domains, because while the lists capture popular
\emph{websites}, our dataset contains counters of \emph{aggregate domain
lookups}. An inferred zero counter \emph{does not mean that no visits took
place} to \emph{websites} for the \emph{non-extended} lists. For example, if you
enter \texttt{www.google.com} or \texttt{www.wikipedia.org} into Tor Browser,
neither \texttt{google.com} nor \texttt{wikipedia.org} are actually connected
to. The recursive resolver of the exit may perform the lookup, but Tor will not,
so it is not captured in our dataset for the non-extended lists. The extended
lists, due to being generated from actual website visits, include domains
typically connected to by Tor Browser. Another example is users visiting direct
links to websites and not entering the domain manually in the browser, such as
when following links from search engines or sent through social media.

When designing our measurements the above detail was not considered. We included
the regular popularity lists for sake of comparison. Ideally the non-extended
lists would have been effective sources for preload lists. This was evidently
not the case for Alexa and Tranco (see later results), but was the case for
Umbrella. So while what we did learn helped us understand the value of using
extended lists to improve cache hits, in hindsight we could have come to the
same conclusion without the same granularity for non-extended lists.

In the second phase of our data collection (see
Section~\ref{sec:long:preload:collection}), we discussed the above detail with
the Tor Research Safety Board and concluded to stop collecting data for
(non-extended) Alexa and Tranco, and to minimize the lists for future collection
to those necessary to determine the longevity of potentially useful preload
lists (based on our findings). Out of an abundance of caution, we will only
share the collected counters for non-extended Alexa and Tranco lists with
researchers for research purposes (the rest of the data is public). The counters
collected during the second phase were consistent with the data from the first
phase.

During the third phase of data collection, we limited the collection to extended
Tranco top-\{10, 100, 1k, 2k, 4k, 5k, 7k, 10k\} lists and the Umbrella top-10k
list, all from April 29. The goal was to learn how cache hits get worse over
time with old lists. Out of 141,624 sample counters collected, three were zero
and 59 were relatively zero when compared to the more popular list.


\subsection{Performance Evaluation} \label{sec:long:evaluation}

The goal of our evaluation is to determine over time:
  cache-hit ratio of potential preload lists (Section~\ref{sec:long:preload:lists}),
  memory usage at exits (Section~\ref{sec:long:preload:entries}), and
  resolver load (Section~\ref{sec:long:preload:resolverload}).

\subsubsection{Results: Preload Lists} \label{sec:long:preload:lists}

\begin{figure}
	\subfloat[][web]{%
		\includegraphics[width=\columnwidth]{img/plot_preload_lists-web.pdf}
		\label{fig:preload:heatmap:web}
	}\\
	\subfloat[][permissive]{%
		\includegraphics[width=\columnwidth]{img/plot_preload_lists-permissive.pdf}
		\label{fig:preload:heatmap:perm}
	}
	\caption{%
	Shared cross-circuit cache-hit ratios (\%) for selected preload lists during
	the first six weeks (x-axis) of data collection. The plotted values are medians over
	24h, and dates on the y-axis show the date of original list download.}
	\label{fig:preload:heatmap}
\end{figure}

Our dataset is extensive with 2,498,424 sample counters from 400 popularity
lists spanning about four months. Figure~\ref{fig:preload:heatmap} shows
comprehensive heatmaps of shared cross-circuit cache-hit ratios for the web
(Figure~\ref{fig:preload:heatmap:web}) and permissive
(Figure~\ref{fig:preload:heatmap:perm}) exits over the first six weeks of data
collection (first and second phases). Cache-hit ratios are medians (very similar
to the mean) for 24h periods. In each figure, the groupings of the four weeks
when we added new lists are visible (top to bottom), as well as baseline Tor at
the bottom row for sake of comparison. Note how the regular Alexa and Tranco
top-10k lists perform poorly: the two black ($<5\%$ cache-hit ratio) lines at
the top of each grouping. Even Umbrella 1k is better, with Umbrella 10k being
comparable to baseline Tor. The extended lists clearly improve over baseline
Tor, with the extended 10k-lists even reaching over 30\% cross-circuit cache-hit
ratios some days. Look at how the lists change over time: we see no real
difference between lists generated at end of April and those generated during
May, but consistent changes across all lists over time, likely due to varying
traffic at the exits. The differences between using Alexa or Tranco to generate
extended lists are negligible, so we focus on Tranco for the remainder of this
analysis as it is open, maintained, and a more recent source of website
popularity~\cite{tranco}.


\begin{figure}
	\subfloat[][web]{%
		\includegraphics[width=\columnwidth]{img/plot_preload_hits-web.pdf}
		\label{fig:preload:hits:web}
	}\\
	\subfloat[][permissive]{%
		\includegraphics[width=\columnwidth]{img/plot_preload_hits-permissive.pdf}
		\label{fig:preload:hits:perm}
	}
	\caption{%
	Shared cross-circuit cache-hit ratios for eight different extended Tranco
	lists, Umbrella top-10k, and Tor baseline during four months in 2022.}
	\label{fig:preload:hits}
\end{figure}

Figure~\ref{fig:preload:hits} shows the observed \emph{cross-circuit} cache-hit
ratios for eight different extended Tranco lists, Umbrella top-10k, and Tor
baseline. We used lists from the end of April because they have the most data.
As a baseline, Tor's current DNS cache has a mean cache-hit ratio of 11\% for
web and 17\% for permissive. In terms of different popularity lists, the regular
(non-extended) Tranco and Alexa lists are ineffective: the top-10k lists are
regularly below $5\%$ for web and permissive (see
Figure~\ref{fig:preload:heatmap}). Umbrella top-10k does much better with mean
17\% (web) and 16\% (permissive). This is slightly worse (permissive) and
comparable (web) to baseline Tor.

The extended lists show a further improvement, comparable in terms of
\emph{average} (full duration of lists) cross-circuit cache-hit ratios to
baseline Tor at top-200 for Alexa and Tranco for web and at top-700 for
permissive. The extended lists from top-1k get (depending on which of the
compiled extended Tranco lists) 20--24\% (web) and 15--18\% (permissive) and up
to 27--32\% (web) and 22--27\% (permissive) at 10k. There is very little gain
between top-7k and top-10k. In general, the extended lists do relatively worse
on the permissive exit and the Tor baseline is higher: this makes sense, since
Alexa and Tranco are focused on websites. This is further confirmed by Umbrella
doing better as a more general-purpose domain popularity list.

Note that Figure~\ref{fig:preload:hits} shows the cross-circuit cache-hit ratios
for a selection of the very first preload lists we created on the April 29. The
results are very encouraging: time seems to have only a slight detrimental
impact on cache hits. After four months the larger extended lists show a
noticable performance improvement over baseline, with the exception of an odd
spike in baseline in early September (we speculate that this is DDoS-related).
The robustness of preload lists removes one of the main downsides of the preload
design, i.e., to maintain and deliver a current list to exits. It is likely
feasible to ship hard-coded preload lists as part of regular Tor releases and
still improve performance, assuming that exit operators upgrade their software a
couple of times per year.

\subsubsection{Results: Cache Entries} \label{sec:long:preload:entries}

\begin{figure}
	\subfloat[][web]{%
		\includegraphics[width=\columnwidth]{img/plot_preload_entries-web.pdf}
		\label{fig:preload:entries:web}
	}\\
	\subfloat[][permissive]{%
		\includegraphics[width=\columnwidth]{img/plot_preload_entries-permissive.pdf}
		\label{fig:preload:entries:perm}
	}
	\caption{%
	Estimated cache entries for eight different extended Tranco lists, Umbrella
	top-10k, and Tor baseline.}
	\label{fig:preload:entries}
\end{figure}

Figure~\ref{fig:preload:entries} shows the number of cache entries needed in Tor
as-is (``baseline Tor'') and for the preload design for a range of different
popularity lists. We can accurately estimate an upper bound because we collected
the total number of entires in all same-circuit caches as part of our
measurements. This count is an upper bound, because some of those entries would
have already been cached in the preload cache. The popularity lists have static
sizes, and to be an accurate upper bound we used the largest observed size for
each list over the four weeks.

Starting with the same-circuit cache, look at the line for extended Tranco
top-10 (``x-Tranco 10'') in Figure~\ref{fig:preload:entries}: this extended list
contains only 90 entries, so the lines at the bottom show mostly the number of
entries used by the same circuit cache. The size of the same-circuit caches
should be proportional to the number of open circuits, and therefore follow exit
probability. Based on the data from Figure~\ref{fig:preload:entries}, we do not
suspect this to be a significant burden. It would be trivial to cap the size
and/or prune the size as part of OOM-management, or dropping entries based on
their age would probably have little impact on performance (presumably most
value is at the start of the circuit when most streams are attached).

Recall from Section~\ref{sec:torcache:metrics} and
Figures~\ref{fig:metrics:cacheweb} and~\ref{fig:metrics:cacheperm} that the
permissive exit had a mean of 12,130 entries compared to the web exit's 7,672
mean. We see the same figures for the baseline in
Figure~\ref{fig:preload:entries}. Albeit slightly higher on average for the web
exit but more stable, we see that Umbrella 10k as well as extended Tranco top-1k
are about the same as Tor baseline. So with about the same memory usage as now
the preload design would offer slightly (permissive) or noticeably (web) better
cache-hit ratios. Looking at the top-2k up until top-10k extended lists we see a
significant higher memory usage (only slightly sublinear) but that comes with
significantly higher cache-hit ratios, as seen in Figure~\ref{fig:preload:hits}.
In absolute terms, for extended Tranco top-10k, about 60,000 cache
entries---even if pessimistically assuming 1 KiB per entry---would end up using
about 60 MiB of memory for the cache. Since domains can be at most 255 bytes and
most domains are much shorter, one could clearly implement the cache
more memory-efficiently. Also, as mentioned earlier, it is likely possible to
reduce the size of the extended top-10k lists by removing useless tracking
domains. Further note that the memory needed to cache the preload list---unlike
the same-circuit cache---only depends on the size of the list, not the number
circuits or streams created at the exit.

\subsubsection{Results: Resolver Load} \label{sec:long:preload:resolverload}

In general, on the one hand, improving cache-hit ratios will reduce resolver load
and scale well with increased traffic. On the other hand, continuously
refreshing domains on the preload list increases resolver load. Consider the
mean number of lookups at the web exit, 17,529, and its mean/median cache-hit
ratio of 0.80 (see Section~\ref{sec:torcache}). This implies an expected
$3.9\gets\frac{17529(1-0.80)}{15\cdot60}$ requests per second to the exit's
resolver.  For the permissive exit we observed about 7.8 requests per second. As
a source of comparison, Sonntag~\cite{sonntag-metrics,Sonntag19} reports for a
DNS resolver dedicated to a 200 Mbit/s exit in 2018 an average of 18.5 requests
per second.

The resolver load for the different preload lists should be proportional to the
estimated number of cache entries shown in Figure~\ref{fig:preload:entries}. The
estimated load for an extended top-1k list would be similar to current Tor,
while the extended top-10k list would see about a seven-fold increase without
changes. This may or may not be problem. Given the variability of lookups we
observed throughout our data collection (Figure~\ref{fig:lookups}) and reported
by Sonntag, resolvers are clearly capable of dealing with increased loads.
Requests due to the preload list should be predictable, consistent, and cheap in
terms of bandwidth even for a low-capacity exit.

Regardless, the load on resolvers could be lowered by reducing the number of
domains, e.g., the increased cache-hit ratio from top-7k to top-10k is very
small ($\approx$1\%) for a 20--30\% increase in entries. One could also increase
the internal TTLs, i.e., the frequency of refreshing the entries in the preload
cache. In Tor, this is especially practical since circuits use random exits. In
the rare case of stale data causing issues, simply create a fresh circuit.
Serving stale data is not uncommon in DNS~\cite{rfc8767}, further discussed next
in Section~\ref{sec:related}.
