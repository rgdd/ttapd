\section{Mitigations} \label{sec:short}

Until a more comprehensive defense can be deployed we propose two short-term
mitigations that require little (fuzzy TTLs) or no (cover lookups) changes to
Tor.  The former adds some uncertainty with regards to when a domain was added
to an exit's DNS cache. The latter can remove or reduce the attacker's ability
to conduct attacks against specific domains but is limited in its scalability.

\subsection{Fuzzy TTLs} \label{sec:short:fuzzy}
Recall that it is possible to determine when a domain was inserted into an
exit's DNS cache (Section~\ref{sec:attack:detailed}) once you know the time $t$
when the timeless timing attack started, the duration until the domain was no
longer cached $x$ (repeated probes), and the expected clip value
$\mathsf{clipped\_ttl}$ of the domain.  The idea of fuzzy TTLs is to add
uncertainty by randomizing the length of time that an entry is cached.

In more detail, keep Tor's DNS cache as-is but sample the cache duration
uniformly at random from $[m, \mathsf{clipped\_ttl}]$, where $m$ is the minimum
duration to cache.  Upon observing the exact time of removal $t+x$, the attacker
now learns that the domain has been in the cache for the duration $x$ and was
thus cached between $[t+x-\mathsf{clipped\_ttl}, t-m]$. Note that if $m =
\mathsf{clipped\_ttl}$, then $x = 0$; the same as in Tor today.

The reality of websites is unfortunately that they consist of multiple domains,
reducing the effectiveness of fuzzy TTLs because the attacker uses the most
lucky sample. For a list of domains $d_1,\hdots,d_k$ that were added at the same
time with identical clips, then $x \leftarrow \mathsf{max}(x_1,\hdots,x_k)$.
Based on our preload list measurements presented in
Section~\ref{sec:long:preload}, we expect around 8--13 domains per site
available for an attacker to potentially query for. Earlier work found
a median of two unique domains out of ten domains in total per website on Alexa
top 1M~\cite{GreschbachPRWF17}.

Fuzzy TTLs are an ineffective mitigation if the attacker just wants to confirm
suspected activity with a low base rate, i.e., the mere existence of cached
domains anywhere in the network is enough of a signal~\cite{wfwo}. Fuzzy TTLs
are a plus for websites that are modestly popular in the network, since the
attacker has to determine which of several exits with cached domains is the
correct one. Having to consider multiple domains and exits (to narrow down the
exact time) is more noisy in the network and increases the risk of
detection~\cite{AumannL07}. Attackers may be forced to consider a time-window of
several seconds or even minutes, which is a big win for defending against
correlation and confirmation attacks~\cite{wfwo,GreschbachPRWF17}.

\subsection{Cover Lookups}
The idea of the cover lookups mitigation is to simply inject domains into DNS
caches in the Tor network to create false positives.   Injected domains must be
indistinguishable from domains cached from real Tor user activity. For this, a
distribution that models website visits for a particular base rate should be
used rather than running, e.g., a deterministic cron job.  Further, care has to
be taken to capture all predictable domains for each website to defend.

A more drastic mitigation would be to keep a site's domains cached at every exit
all the time, e.g., by running \texttt{exitmap}~\cite{exitmap} every five
minutes. This obviously scales poorly.  The network overhead would already be
significant for a few hundred sites, e.g., estimates based on Alexa top-1k would
add about 26.7~requests per second to each exit.

Cover lookups do not scale, even if just injected at few exits probabilistically
according to some target base rate. It is a last resort mitigation for site
operators that fear that their users are targeted by motivated attackers and
where, for some reason, the site cannot transition to being completely (no
resources loaded from other domains) hosted as an onion service.
