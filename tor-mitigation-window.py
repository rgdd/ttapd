#!/usr/bin/env python3

import random

# from Tor's clip on DNS TTLs: we assume that the TTLs are all clipped to the
# MIN_DNS_TTL in this example
MIN_DNS_TTL = 5*60
#MAX_DNS_TTL = 60*60

FUZZY_DNS_TTL = 4*60
MIN_CLIPPED = MIN_DNS_TTL - FUZZY_DNS_TTL
MAX_CLIPPED = MIN_DNS_TTL + FUZZY_DNS_TTL

print(f"MIN_DNS_TTL: {MIN_DNS_TTL}")
print(f"FUZZY_DNS_TTL: {FUZZY_DNS_TTL}")
print(f"MIN_CLIPPED: {MIN_CLIPPED}")
print(f"MAX_CLIPPED: {MAX_CLIPPED}")

# the patched clip function in Tor is as follows:
# clip_dns_fuzzy_ttl(uint32_t ttl)
# {
#   return clip_dns_ttl(ttl) +
#     crypto_rand_uint(1 + 2*FUZZY_DNS_TTL) - FUZZY_DNS_TTL;
# }
def clip_dns_ttl(ttl):
    return ttl + random.randint(0, 2*FUZZY_DNS_TTL) - FUZZY_DNS_TTL

REPS = 10000
DOMAINS = 13

print("an attacker has to consider the following durations for the website visit:")
for d in range(1,DOMAINS+1):
    window = []
    for _ in range(REPS):
        # domains are cached to expire according to clip: our attack still
        # works, so we can observe each cached expiry timestamp (assuming that
        # the visit took place at time 0)
        cached_expiry_timestamps = [clip_dns_ttl(MIN_DNS_TTL) for j in range(d)]

        # OK, so we observed a bunch of expirations. We assume that all of the
        # entries were cached as a result of the same visit, AND (implicitly)
        # that our attack started before any entry expired (i.e., before
        # MIN_CLIPPED), AND that we're only dealing with original 300s
        # MIN_DNS_TTL. When the visit took place, all expiration timestamps were
        # placed [MIN_CLIPPED, MAX_CLIPPED] sections in the future. Using the
        # attack, we observed these expirations, as in cached_expiry_timestamps.
        first_expiry = min(cached_expiry_timestamps)
        last_expiry = max(cached_expiry_timestamps)

        # reminder: our goal is to find the bounds for when the visit happened

        # the earliest possible visit is the last expiry and assuming that it
        # sampled the MAX_CLIPPED duration
        earliest_possible_visit = last_expiry - MAX_CLIPPED

        # the latest possible visit is the last expiry and assuming that it
        # sampled the MAX_CLIPPED duration
        latest_possible_visit = first_expiry - MIN_CLIPPED

        #print(f"visit happened between timestamps {earliest_possible_visit} and
        #{latest_possible_visit}")
        window.append(abs(latest_possible_visit-earliest_possible_visit))
    
    print(f"\t{d:2} domains, avg window {sum(window)/len(window):5.1f} seconds")
