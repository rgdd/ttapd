#!/usr/bin/env python3

#
# Usage: carml cmd 'getinfo ns/all' | ./exit-relays.py 2>stderr >stdout
#

import sys
import argparse

import logging
log = logging.getLogger(__name__)

def parse_args():
    parser = argparse.ArgumentParser(description="parse the names of all running exit relays")
    return parser.parse_args()

def main(args):
    exit2count = {}
    name = None
    for line in sys.stdin:
        line = line.rstrip()
        if line.startswith("r"):
            if name is not None:
                log.critical("expected exit relay name to be unset")
                sys.exit(1)
            try:
                name = line.split(" ")[1]
            except Exception as err:
                log.critical(f"expected exit relay on line: {line}")
                sys.exit(1)

        if line.startswith("s"):
            if name is None:
                log.critical(f"expected exit relay name to be set")
                sys.exit(1)
            if "Exit" in line:
                exit2count.setdefault(name, 0)
                exit2count[name] += 1
            name = None

    for name in exit2count:
        if exit2count[name] != 1:
            log.warning(f"skipping duplicate name {name}")
            continue
        print(name)

if __name__ == "__main__":
    logging.basicConfig(
        format="[%(levelname)s] %(asctime)s: %(message)s",
        level=logging.DEBUG,
    )
    sys.exit(main(parse_args()))
