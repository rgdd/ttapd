#!/usr/bin/env python3

import sys
import argparse
import datetime

import logging
log = logging.getLogger(__name__)

def parse_args():
    parser = argparse.ArgumentParser(
        description="views collected attack metrics",
    )
    parser.add_argument('-f', "--file",
        type=str, required=True, nargs='+',
        help="path to one or more files with collected data",
    )
    return parser.parse_args()

def main(args):
    log.info(f"using data files: {args.file}")
    num_run = 0
    min_timestamp, max_timestamp = 2**64 - 1, 0
    uncached_ok, uncached_nok, uncached_unknown = 0, 0, 0
    cached_ok, cached_nok, cached_unknown = 0, 0, 0
    for fp in args.file:
        with open(fp, "r") as f:
            for line in f:
                relay, timestamp, data = line.split(" ")
                num_run += 1

                if min_timestamp > int(timestamp):
                    min_timestamp = int(timestamp)
                if max_timestamp < int(timestamp):
                    max_timestamp = int(timestamp)

                values = data.split(",")
                for i in range(len(values)):
                    value = int(values[i])
                    if i%2 == 0:
                        if value == 0:
                            uncached_ok += 1
                        elif value == 1:
                            uncached_nok += 1
                        else:
                            uncached_unknown += 1
                    else:
                        if value == 1:
                            cached_ok += 1
                        elif value == 0:
                            cached_nok += 1
                        else:
                            cached_unknown += 1

    num_uncached = uncached_ok + uncached_nok + uncached_unknown
    num_cached = cached_ok + cached_nok + cached_unknown
    print(f"start: {datetime.datetime.fromtimestamp(min_timestamp)}")
    print(f"stop:  {datetime.datetime.fromtimestamp(max_timestamp)}")
    print(f"num run: {num_run} ({len(args.file)} runners)")
    print(f"qps tor: { 2*(num_uncached + num_cached) / (max_timestamp-min_timestamp):.1f}")
    print(f"qps dns: { num_uncached / (max_timestamp-min_timestamp):.1f}")
    print(f"uncached look-ups")
    print(f"   ok: {uncached_ok}")
    print(f"  nok: {uncached_nok}")
    print(f"  ???: {uncached_unknown}")
    print(f"  sum: {uncached_ok + uncached_nok + uncached_unknown}")
    print(f"cached look-ups")
    print(f"   ok: {cached_ok}")
    print(f"  nok: {cached_nok}")
    print(f"  ???: {cached_unknown}")
    print(f"  sum: {cached_ok + cached_nok + cached_unknown}")

if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s %(name)s [%(levelname)s] %(message)s",
        level=logging.DEBUG,
    )
    sys.exit(main(parse_args()))
