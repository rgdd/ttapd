#!/bin/bash

#
# Usage
#
#   ./run.sh LOG_DIR SOCKS_PORT CONTROL_PORT DOMAIN_NAME NUM_TEST 2>run.info >run.data
#
# PRE:
#
#   - tor must contain our timeless timing attack patch
#   - tor must run with control port set to 9051
#   - at least python3, carml, and tor-resolve must be installed
#

set -eu

log_dir=$1; shift
socks_port=$1; shift
control_port=$1; shift
base_dn=$1; shift
num_test=$1; shift
rid=$RANDOM

function info() { echo "$(date +"%Y-%m-%d %H:%M:%S") [INFO] $@" >&2; }
function warn() { echo "$(date +"%Y-%m-%d %H:%M:%S") [WARN] $@" >&2; }

function names() {
	output=""
	now=$(date +%s)
	for i in `seq 1 $num_test`; do
		output="$output$(echo -n $i.$rid.$now.$base_dn)," # first uncached
		output="$output$(echo -n $i.$rid.$now.$base_dn)," # then cached
	done
	echo ${output::-1}
}

mkdir -p $log_dir
info "writing logs to $log_dir"
info "base domain is $base_dn"
info "num test is $num_test"
info "random id is $rid"

num_run=0
while true; do
	num_run=$((num_run+1))
	info "run $num_run: waiting five seconds before start, ctrl+c to cancel"
	sleep 5

	carml cmd "getinfo ns/all" | ./exit-relays.py 2>$log_dir/exit-relays.info >$log_dir/exit-relays.data
	exit_name=$(cat $log_dir/exit-relays.data | shuf | head -n1)
	if [[ -z $exit_name ]]; then
		warn "name of exit relay is empty"
		continue
	fi

	info "selected exit relay $exit_name"
	if [[ $exit_name == DFRI11 ]] || [[ $exit_name == DFRI12 ]]; then
		info "skipping relay with other concurrent measurements"
		continue
	fi

	timestamp=$(date +%s)
	if ! ./attack.py -d $log_dir -s $socks_port -c $control_port -r $exit_name -n $(names) 2>$log_dir/attack.info >$log_dir/attack.data; then
		warn "attack: non-zero return code, see $log_dir/$timestamp.info"
		mv $log_dir/attack.info $log_dir/$timestamp.info
		continue
	fi

	echo "$exit_name $timestamp $(cat $log_dir/attack.data)"
done
