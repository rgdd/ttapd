#!/usr/bin/env python3

import argparse
import datetime
import json
import math
import matplotlib
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

ap = argparse.ArgumentParser()
ap.add_argument("-p", type=str, nargs="+",
    default=["data/DFRI11-ext.dat", "data/DFRI11-weights.json"],
    help="paths to data file and weights json corresponding to permissive ports",
)
ap.add_argument("-w", type=str, nargs="+",
    default=["data/DFRI12-ext.dat", "data/DFRI12-weights.json"],
    help="paths to data file and weights json corresponding to web ports",
)
ap.add_argument("-c", type=str,
    default="/etc/tor/tlwo/artifact/preload-lists/generated/",
    help="common list identifier that can be removed",
)
args = vars(ap.parse_args())

def main():
    sns.set_theme(style="dark")
    sns.set_palette("colorblind")
    sns.set_context("talk")
    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["ps.fonttype"] = 42

    for file_path in [ args["p"], args["w"] ]:
        data = read(file_path, args["c"])
        data["fname"] = "web" if "DFRI12" in file_path[0] else "permissive"

        # print(len(data["timestamp"]))
        # print(len(data["lookups"]))
        # print(len(data["hits_5m"]))
        # print(len(data["hits_60m"]))
        # print(len(data["hits_pending"]))
        # print(len(data["hits_same_circuit"]))
        # print(len(data["num_same_circuit_entries"]))
        # print(len(data["num_cache_entries"]))

        # figures for the metrics section:
        # - total lookups and exit probability for both exits, over time
        plot_lookups(data)
        # - num_cache_entries for both exits, over time
        plot_cache_entries(data)
        # 3+4. one per exit: fraction, hits total (hits_5m + hits_60m +
        # hits_pending), hits same circuit, hits global, area
        plot_cache_hits(data)
        
        # heatmap for preload lists
        plot_preload_lists(data)

        # memory comparison, in terms of #entries
        plot_preload_entries(data)

        # preload cache hits (first list)
        plot_preload_hits(data)

        # popularity matching, NOTE: lacking data to plot in extended collection
        plot_popularity_match(data)

def plot_popularity_match(data):
    print("plot_popularity_match")
    fig, ax1 = plt.subplots()
    fig.set_size_inches(6,3)

    alexa = []
    tranco = []
    xalexa = []
    xtranco = []
    umbrella = []    
    #points = [10, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000]
    points = [10, 100, 1000, 10000]
    for p in points:
        lookups = np.mean(np.array(data["lookups"]))
        prefix = "2022-04-29/"
        m = np.mean(np.array(data[f"{prefix}alexa-top-{p}"]))
        alexa += [ m ]
        m = np.mean(np.array(data[f"{prefix}tranco-top-{p}"]))
        tranco += [ m ]
        m  = np.mean(np.array(data[f"{prefix}ext-alexa-top-{p}"]))
        xalexa += [ m ]
        m = np.mean(np.array(data[f"{prefix}ext-tranco-top-{p}"]))
        xtranco += [ m ]
        m  = np.mean(np.array(data[f"{prefix}umbrella-top-{p}"]))
        umbrella += [ m ]

    for i in range(1,len(alexa)):
        alexa[i] -= alexa[i-1]
        tranco[i] -= tranco[i-1]
        xalexa[i] -= xalexa[i-1]
        xtranco[i] -= xtranco[i-1]
        umbrella[i] -= umbrella[i-1]

    salexa = np.sum(alexa)
    stranco = np.sum(tranco)
    sxalexa = np.sum(xalexa)
    sxtranco = np.sum(xtranco)
    sumbrella = np.sum(umbrella)

    for i in range(len(alexa)):
        alexa[i] = alexa[i] / salexa
        tranco[i] = tranco[i] / stranco
        xalexa[i] = xalexa[i] / sxalexa
        xtranco[i] = xtranco[i] / sxtranco
        umbrella[i] = umbrella[i] / sumbrella

    print(alexa)
    print(tranco)
    print(xalexa)
    print(xtranco)
    print(umbrella)

    mani = [8.4/24, 5.1/24, 6.2/24, 4.3/24]

    tmpdata = {}
    tmpdata["rank"] = []
    tmpdata["list"] = []
    tmpdata["frac"] = []
    for i in range(len(alexa)):
        tmpdata["list"] += [ "Mani et al." ]
        tmpdata["rank"] += [ points[i] ]
        tmpdata["frac"] += [ mani[i] ]
        tmpdata["list"] += [ "Alexa" ]
        tmpdata["rank"] += [ points[i] ]
        tmpdata["frac"] += [ alexa[i] ]
        tmpdata["list"] += [ "Tranco" ]
        tmpdata["rank"] += [ points[i] ]
        tmpdata["frac"] += [ tranco[i] ]
        tmpdata["list"] += [ "Umbrella" ]
        tmpdata["rank"] += [ points[i] ]
        tmpdata["frac"] += [ umbrella[i] ]
        tmpdata["list"] += [ "x-Alexa" ]
        tmpdata["rank"] += [ points[i] ]
        tmpdata["frac"] += [ xalexa[i] ]
        tmpdata["list"] += [ "x-Tranco" ]
        tmpdata["rank"] += [ points[i] ]
        tmpdata["frac"] += [ xtranco[i] ]


    #d = pd.DataFrame(zip(alexa, tranco, umbrella, xalexa, xtranco, mani),
    #    points,
    #    columns=["Alexa", "Tranco", "Umbrella", "x-Alexa", "x-Tranco", "Mani et al."])
    d = pd.DataFrame(tmpdata)
    print(d)

    sns.barplot(data=d, x="rank", y="frac", hue="list", ax=ax1)
    ax1.set_xlabel("rank")
    ax1.set_ylabel("ratio of lookups")
    ax1.set_xticklabels(["(0,10]", "(10, 100]", "(100,1k]", "(1k,10k]"])
    plt.legend(bbox_to_anchor=(1.02, 1), loc=2, borderaxespad=0.).get_frame().set_linewidth(0.0)
    plt.savefig(f"plot_popularity_match-{data['fname']}.pdf", bbox_inches='tight') 

def plot_preload_hits(data):
    print("plot_preload_hits")
    fig, ax1 = plt.subplots()
    fig.set_size_inches(6,3)
    tmpdata = {}
    tmpdata["timestamp"] = []
    tmpdata["hits_global"] = []
    poplist = [
        "2022-04-29/ext-tranco-top-10000",
        #"2022-04-29/ext-tranco-top-9000",
        #"2022-04-29/ext-tranco-top-8000",
        "2022-04-29/ext-tranco-top-7000",
        #"2022-04-29/ext-tranco-top-6000",
        "2022-04-29/ext-tranco-top-5000",
        "2022-04-29/ext-tranco-top-4000",
        #"2022-04-29/ext-tranco-top-3000",
        "2022-04-29/ext-tranco-top-2000", "2022-04-29/ext-tranco-top-1000",
        "2022-04-29/umbrella-top-10000",
        "2022-04-29/ext-tranco-top-100", "2022-04-29/ext-tranco-top-10",
    ]
    poplabel = [
        "x-Tranco 10k",
        #"x-Tranco 9k",
        # "x-Tranco 8k",
        "x-Tranco 7k",
        #"x-Tranco 6k",
        "x-Tranco 5k",
        "x-Tranco 4k",
        #"x-Tranco 3k",
        "x-Tranco 2k", "x-Tranco 1k",
        "Umbrella 10k",
        "x-Tranco 100", "x-Tranco 10",
    ]
    for p in poplist:
        tmpdata[p] = []
    for i, t in enumerate(data["timestamp"]):
        tmpdata["timestamp"] += [ humantime(t, 60*60*24) ]
        total = data["hits_5m"][i] + data["hits_60m"][i] + data["hits_pending"][i]
        tmpdata["hits_global"] += [ (total - data["hits_same_circuit"][i]) / data["lookups"][i] ]

        for p in poplist:
            tmpdata[p] += [ data[p][i] / data["lookups"][i] ]

    zargs = [tmpdata[f] for f in poplist]
    zargs += [tmpdata["hits_global"]]
    zcol = [f for f in poplabel]
    zcol += ["baseline Tor"]

    # legend fix: move baseline up two spots, ugly
    a = len(zargs) -1 # last
    b = a - 2 # two up
    zargs[a], zargs[b] = zargs[b], zargs[a]
    zcol[a], zcol[b] = zcol[b], zcol[a]
    b = a -1
    zargs[a], zargs[b] = zargs[b], zargs[a]
    zcol[a], zcol[b] = zcol[b], zcol[a]


    d = pd.DataFrame(zip(*zargs),
        tmpdata["timestamp"],
        columns=zcol)

    sns.lineplot(data=d, linewidth=2.5, ax=ax1, dashes=True)
    ax1.set_xticks(xticks)
    ax1.set_xlabel("")
    ax1.set_ylabel("cache hit ratio")
    ax1.set_xticklabels(xlabels)
    plt.legend(bbox_to_anchor=(1.02, 1), loc=2, fontsize="x-small", borderaxespad=0.).get_frame().set_linewidth(0.0)
    plt.savefig(f"plot_preload_hits-{data['fname']}.pdf", bbox_inches='tight')

def plot_preload_entries(data):
    print("plot_preload_entries")
    fig, ax1 = plt.subplots()
    fig.set_size_inches(6,3)
    tmpdata = {}


    n = len([d/1000 for d in data["num_cache_entries"] ])
    tmpdata["entries"] = []
    tmpdata["timestamps"] = []
    tmpdata["cache"] = []

    def add(name, diff):
        tmpdata["entries"] += [ (t + diff)/1000 for t in data["num_same_circuit_entries"] ]
        tmpdata["timestamps"] += [humantime(t, 60*60*24) for t in data["timestamp"][-n:]]
        tmpdata["cache"] += [name] * n

    #add("x-Alexa 10", 105) # 2022-05-06/ext-alexa-top-10
    #add("x-Alexa 100", 1609) # 2022-05-06/ext-alexa-top-100
    #add("x-Alexa 1k", 9346) # 2022-04-29/ext-alexa-top-1000
    #add("x-Alexa 10k", 54712) # 2022-05-13/ext-alexa-top-10000
    
    add("x-Tranco 10k", 53947) # 2022-04-29/ext-tranco-top-10000
    add("x-Tranco 7k", 40638) # 2022-04-29/ext-tranco-top-7000
    add("x-Tranco 5k", 31223) # 2022-04-29/ext-tranco-top-5000
    add("x-Tranco 4k", 26309) # 2022-04-29/ext-tranco-top-4000
    add("x-Tranco 2k", 15480) # 2022-04-29/ext-tranco-top-2000
    add("Umbrella 10k", 10000) # by def
    add("x-Tranco 1k", 8872) # 2022-04-29/ext-tranco-top-1000
    tmpdata["entries"] +=  [d/1000 for d in data["num_cache_entries"] ]
    tmpdata["timestamps"] += [ humantime(t) for t in data["timestamp"][-n:] ]
    tmpdata["cache"] += ["baseline Tor"] * n
    add("x-Tranco 100", 1386) # 2022-04-29/ext-tranco-top-100
    add("x-Tranco 10", 90) # 2022-05-06/ext-tranco-top-10

    sns.lineplot(x="timestamps", y="entries", hue="cache", style="cache", dashes=True, data=tmpdata, ax=ax1)
    ax1.set_xticks([datetime.datetime(2022, 5, 1), datetime.datetime(2022, 6, 1), datetime.datetime(2022, 7, 1), datetime.datetime(2022, 8, 1), datetime.datetime(2022, 9, 1)])
    ax1.set_xticklabels(["May 1", "June 1", "July 1", "Aug 1", "Sep 1"])
    ax1.set_xlabel("")
    ax1.set_ylabel("entries (k)")
    plt.legend(bbox_to_anchor=(1.02, 1), loc=2, fontsize="x-small", borderaxespad=0.).get_frame().set_linewidth(0.0)
    ax1.yaxis.set_major_formatter(matplotlib.ticker.StrMethodFormatter('{x:,.0f}'))
    plt.savefig(f"plot_preload_entries-{data['fname']}.pdf", bbox_inches='tight')

def plot_preload_lists(data):
    print("plot_preload_lists")
    fig, ax = plt.subplots()
    fig.set_size_inches(10,14)

    def prettylist(k):
        k = k.replace("2022-", "")
        k = k.replace("04-", "April ")
        k = k.replace("05-", "May ")
        k = k.replace("-", " ")
        k = k.replace("/alexa", ", Alexa")
        k = k.replace("/ext alexa", ", x-Alexa")
        k = k.replace("/tranco", ", Tranco")
        k = k.replace("/ext tranco", ", x-Tranco")
        k = k.replace("/umbrella", ", Umbrella")
        k = k.replace("top", "")
        k = k.replace("10000", "10k")
        k = k.replace("1000", "1k")
        #k = f"{k:>30}"
        return k

    # how far in time was phase2
    phase2 = math.floor(len(data["2022-05-20/umbrella-top-1000"]))*2

    l, t, f = [], [], []
    for i, _ in enumerate(data["timestamp"][:phase2]):
        l += [ "Tor baseline" ]
        t += [ humantime(data["timestamp"][i], 60*60*24).strftime("%m %d") ]
        total = data["hits_5m"][i] + data["hits_60m"][i] + data["hits_pending"][i]
        f += [ int(100*(total - data["hits_same_circuit"][i]) / data["lookups"][i]) ]

    for k in data:
        if is_preload_list(k):
            if "100" in k and not k.endswith("alexa-top-100") and not k.endswith("tranco-top-100") and not k.endswith("/alexa-top-1000") and not k.endswith("/tranco-top-100") and not k.endswith("/tranco-top-1000") and not k.endswith("umbrella-top-100"):
                offset = data["first_seen"][k]
                for i, v in enumerate(data[k][:phase2]):
                    l += [ prettylist(k) ]
                    t += [ humantime(data["timestamp"][offset+i], 60*60*24).strftime("%m %d") ]
                    f += [ int(100*v / data["lookups"][offset+i]) ]

    d = pd.DataFrame(zip(l, t, f),
        columns=["list", "time", "hit rate"])
    d = d.pivot_table(index="list", columns="time", values="hit rate", aggfunc="median")
    xlong = ["May 2", "", "", "", "", "", "", "", "", "", "", "", "", "", "May 16", "", "", "", "", "", "", "", "", "", "", "", "", "", "May 30", "", "", "", "", "", "", "", "", "", "", "", "", "", "June 13"]
    sns.heatmap(d, ax=ax, xticklabels=xlong, annot=False, fmt="0.0f", center=0, vmin=0.0, vmax=40.0)
    ax.set_xlabel("")
    ax.set_ylabel("")
    plt.savefig(f"plot_preload_lists-{data['fname']}.pdf", bbox_inches='tight')

#xticks = [datetime.datetime(2022, 5, 2), datetime.datetime(2022, 5, 9), datetime.datetime(2022, 5, 16), datetime.datetime(2022, 5, 23),  datetime.datetime(2022, 5, 29)]
#xlabels = ["May 2", "May 9", "May 16", "May 23", "May 29"]
xticks = [datetime.datetime(2022, 5, 1), datetime.datetime(2022, 6, 1), datetime.datetime(2022, 7, 1), datetime.datetime(2022, 8, 1), datetime.datetime(2022, 9, 1)]
xlabels = ["May 1", "June 1", "July 1", "Aug 1", "Sep 1"]

def plot_lookups(data):
    print("plot_lookups")
    al = stats(np.array(data["lookups"]))
    ax = stats(np.array(data["exit_probability"]))
    print(f"\t{al = }, {ax =}")
    # tmp remove spikes from lookup, FIXME make sure documented and quantified
    origlookups = data["lookups"]
    
    lookups = np.array(data["lookups"])
    data["lookups"] = list(lookups)
    cutpoint = 1000
    lookups[lookups < cutpoint] = cutpoint
    cuts = 0
    for d in data["lookups"]:
        if d < cutpoint:
            cuts += 1
    print(f"{cuts = }")

    lookups = lookups / 1000
    data["lookups"] = list(lookups)

    fig, ax1 = plt.subplots()
    fig.set_size_inches(6,3)
    ax2 = ax1.twinx()
    g1 = sns.lineplot(
        data=data,
        x="daytime", y="lookups", estimator="mean",
        ax=ax1, color=sns.color_palette()[0],
    )
    g2 = sns.lineplot(
        data=data,
        x="exit_probability_time", y="exit_probability",
        ax=ax2, color=sns.color_palette()[1],
    )
    g1.set(yscale='log')
    #g2.set(yscale='log')
    ax1.set_xlabel("")
    ax1.set_xticks(xticks)
    ax1.set_xticklabels(xlabels)
    
    ax1.set_ylabel("lookups (k, log)", color=sns.color_palette()[0])
    ax2.set_ylabel("exit probability (%)", color=sns.color_palette()[1])
    ax1.yaxis.set_major_formatter(matplotlib.ticker.StrMethodFormatter('{x:,.0f}'))
    plt.savefig(f"plot_lookups-{data['fname']}.pdf", bbox_inches='tight')

    data["lookups"] = origlookups

    lookups_24 = []
    c = 4*24
    for i in range(len(data["exit_probability_time"])):
        d = np.average(data["lookups"][i*c:(i+1)*c])
        lookups_24.append(d)
    print(f"Pearson correlation coefficient: {np.corrcoef(lookups_24, data['exit_probability'])}")

def plot_cache_entries(data):
    print("plot_cache_entries")
    fig, ax1 = plt.subplots()
    fig.set_size_inches(6,2.5)
    tmpdata = {}
    tmpdata["entries"] = data["num_cache_entries"]
    tmpdata["entries"] = list(np.array(tmpdata["entries"]) / 1000)
    tmpdata["timestamps"] = [humantime(t, 60*60*24) for t in data["timestamp"][-len(tmpdata["entries"]):]]

    sns.lineplot(x="timestamps", y="entries", data=tmpdata, ax=ax1)
    ax1.set_xticks([datetime.datetime(2022, 5, 1), datetime.datetime(2022, 6, 1), datetime.datetime(2022, 7, 1), datetime.datetime(2022, 8, 1), datetime.datetime(2022, 9, 1)])
    ax1.set_xticklabels(["May 1", "June 1", "July 1", "Aug 1", "Sep 1"])
    ax1.set_xlabel("")
    ax1.set_ylabel("entries (k)")
    #ax1.set_yticks([0,25,50,75,100])
    ax1.yaxis.set_major_formatter(matplotlib.ticker.StrMethodFormatter('{x:,.0f}'))
    plt.savefig(f"plot_cache_entries-{data['fname']}.pdf", bbox_inches='tight')

def plot_cache_hits(data):
    print("plot_cache_hits")
    fig, ax1 = plt.subplots()
    fig.set_size_inches(6,3)
    tmpdata = {}
    tmpdata["timestamp"] = []
    tmpdata["hits_total"] = []
    tmpdata["hits_same"] = []
    tmpdata["hits_global"] = []
    tmpdata["hits_5m"] = []
    tmpdata["hits_60m"] = []
    tmpdata["hits_pending"] = []
    for i, t in enumerate(data["timestamp"]):
        tmpdata["timestamp"] += [ humantime(t, 60*60*24) ]
        total = data["hits_5m"][i] + data["hits_60m"][i] + data["hits_pending"][i]
        tmpdata["hits_total"] += [ total / data["lookups"][i] ]
        tmpdata["hits_same"] += [ data["hits_same_circuit"][i] / data["lookups"][i] ]
        tmpdata["hits_global"] += [ (total - data["hits_same_circuit"][i]) / data["lookups"][i] ]

        tmpdata["hits_5m"] += [ data["hits_5m"][i] / data["lookups"][i] ]
        tmpdata["hits_60m"] += [ data["hits_60m"][i] / data["lookups"][i] ]
        tmpdata["hits_pending"] += [ data["hits_pending"][i] / data["lookups"][i] ]

    d = pd.DataFrame(zip(tmpdata["hits_total"], tmpdata["hits_same"], tmpdata["hits_60m"],
        tmpdata["hits_5m"], tmpdata["hits_global"], tmpdata["hits_pending"]),
        tmpdata["timestamp"],
        columns=["total", "same", "60min",
        "5min", "shared", "pending"])

    sns.lineplot(data=d, linewidth=2, ax=ax1, dashes=True)
    ax1.set_xticks(xticks)
    ax1.set_xlabel("")
    ax1.set_ylabel("cache hit ratio")
    ax1.set_xticklabels(xlabels)
    plt.legend(bbox_to_anchor=(1.02, 1), loc=2, borderaxespad=0.).get_frame().set_linewidth(0.0)
    plt.savefig(f"plot_cache_hits-{data['fname']}.pdf", bbox_inches='tight')

def read(paths, common_list_id):
    data = {}
    first_seen = {}
    # read network measurements
    with open(paths[0], "r") as fp:
        i = 0
        for line in fp:
            for kv in line.split(" "):
                split = kv.split("=")
                key = fmt(split[0], common_list_id)
                value = int(split[1])

                if key == "timestamp" and value in [ 1652098834, 1652099734 ]:
                    print(f"skip line with timestamp {value} (lists were misconfigured)")
                    break
                # skip entries befor May 2
                if key == "timestamp" and (value <= 1651449600):
                    break
                #if key == "timestamp" and (value > 1661990400):
                #    break

                if "top" in key and key not in first_seen:
                    first_seen[key] = len(data["lookups"]) - 1

                data.setdefault(key, [])
                data[key] += [ value ]

            if len(data["timestamp"]) != len(data["lookups"]):
                print(f"FIXME: broken data on line {i} in {paths[0]}, go inspect and delete")
                break
            i += 1

    data["first_seen"] = first_seen
    data["humantime"] = [humantime(t) for t in data["timestamp"]]
    data["daytime"] = [humantime(t, 60*60*24) for t in data["timestamp"]]
    print(f"read {len(data['timestamp'])} lines from {paths[0]} ({dates(data['timestamp'])})")

    # read consensus weights from when our measurements data starts
    with open(paths[1], "r") as fp:
        j = json.load(fp)
        first = j["relays"][0]["exit_probability"]["6_months"]["first"]
        factor = float(j["relays"][0]["exit_probability"]["6_months"]["factor"])
        values = j["relays"][0]["exit_probability"]["6_months"]["values"]
        first_time = datetime.datetime.strptime(first, "%Y-%m-%d %H:%M:%S")
        current = first_time
        i = 0
        for v in values:
            i += 1
            # only add probabilities at or after measurement data
            if current >= data["humantime"][0] and current <= data["humantime"][-1]:
                prob = 0.0
                if v is not None:
                    prob = factor*v*100
                data.setdefault("exit_probability", [])
                data["exit_probability"] += [ prob ]
                data.setdefault("exit_probability_time", [])
                data["exit_probability_time"] += [ current ]
            current = current + datetime.timedelta(hours=24)

    return data

def is_preload_list(key):
    return key[0].isnumeric()

def fmt(key, pattern):
    return key[len(pattern):] if key.startswith(pattern) else key

def humantime(t, r=3600):
    rounded = t - ( t % r)
    return datetime.datetime.fromtimestamp(rounded)

def dates(timestamps):
    start = datetime.datetime.fromtimestamp(timestamps[0])
    end = datetime.datetime.fromtimestamp(timestamps[-1])
    return f"{start} - {end}"

def stats(values):
    minval = np.min(values)
    maxval = np.max(values)
    mean = np.mean(values)
    median = np.median(values)
    return f"min:{minval:.3f} max:{maxval:.3f} mean:{mean:.3f} median:{median:.3f}"

if __name__ == "__main__":
    main()