#!/usr/bin/env python3

import argparse
import logging
import sys
import math
import datetime
import numpy as np

log = logging.getLogger(__name__)

def main(args):
    for file_path in [ args.permissive_ports, args.web_ports ]:
        data = read(file_path, args.common_list_id)

        lookups = np.array(data["lookups"])
        hits_5m = np.array(data["hits_5m"])
        hits_60m = np.array(data["hits_60m"])
        hits_pending = np.array(data["hits_pending"])
        hits_same_circuit = np.array(data["hits_same_circuit"])
        num_cache_entries = np.array(data["num_cache_entries"])
        num_same_circuit_entries = np.array(data["num_same_circuit_entries"])

        hits = np.add(np.add(hits_5m, hits_60m), hits_pending)
        hits_not_same_circuit = np.subtract(hits, hits_same_circuit)
        hits_ratio = np.divide(hits, lookups)
        hits_not_same_circuit_ratio = np.divide(hits_not_same_circuit, lookups)

        log.info(f"{'num lookups':30} {stats(lookups)}")
        s = np.sort(lookups)
        log.info(f"\t{len(lookups)} lookups")
        log.info(f"\t{s[:50]}")
        log.info(f"{'hits 5m':30} {stats(np.divide(hits_5m, lookups))}")
        log.info(f"{'hits 60m':30} {stats(np.divide(hits_60m, lookups))}")
        log.info(f"{'hits pending':30} {stats(np.divide(hits_pending, lookups))}")
        log.info(f"{'hits same circuit':30} {stats(np.divide(hits_same_circuit, lookups))}")
        log.info(f"{'hits ratio':30} {stats(hits_ratio)}")
        log.info(f"{'hits ratio not same circuit':30} {stats(hits_not_same_circuit_ratio)}")
        log.info(f"{'num cache entries':30} {stats(num_cache_entries)}")
        log.info(f"{'num same circuit entries':30} {stats(num_same_circuit_entries)}")
        log.info("")

        log.info(f"{'MIN_ROW: lookups':30} {lookups[np.argmin(lookups)]:.3f}")
        log.info(f"{'MIN_ROW: hits_5m':30} {hits_5m[np.argmin(lookups)] / lookups[np.argmin(lookups)]:.3f}")
        log.info(f"{'MIN_ROW: hits_60m':30} {hits_60m[np.argmin(lookups)] / lookups[np.argmin(lookups)]:.3f}")
        log.info(f"{'MIN_ROW: hits_pending':30} {hits_pending[np.argmin(lookups)] / lookups[np.argmin(lookups)]:.3f}")
        log.info(f"{'MIN_ROW: hits_same_circuit':30} {hits_same_circuit[np.argmin(lookups)] / lookups[np.argmin(lookups)]:.3f}")
        log.info(f"{'MIN_ROW: hits':30} {hits[np.argmin(lookups)] / lookups[np.argmin(lookups)]:.3f}")
        log.info(f"{'MIN_ROW: hits not same circuit':30} {hits_not_same_circuit[np.argmin(lookups)] / lookups[np.argmin(lookups)]:.3f}")
        log.info("")

        log.info(f"{'MAX_ROW: lookups':30} {lookups[np.argmax(lookups)]:.3f}")
        log.info(f"{'MAX ROW: hits_5m':30} {hits_5m[np.argmax(lookups)] / lookups[np.argmax(lookups)]:.3f}")
        log.info(f"{'MAX ROW: hits_60m':30} {hits_60m[np.argmax(lookups)] / lookups[np.argmax(lookups)]:.3f}")
        log.info(f"{'MAX ROW: hits_pending':30} {hits_pending[np.argmax(lookups)] / lookups[np.argmax(lookups)]:.3f}")
        log.info(f"{'MAX ROW: hits_same_circuit':30} {hits_same_circuit[np.argmax(lookups)] / lookups[np.argmax(lookups)]:.3f}")
        log.info(f"{'MAX ROW: hits':30} {hits[np.argmax(lookups)] / lookups[np.argmax(lookups)]:.3f}")
        log.info(f"{'MAX ROW: hits not same circuit':30} {hits_not_same_circuit[np.argmax(lookups)] / lookups[np.argmax(lookups)]:.3f}")
        log.info("")

        def has_zero(values):
            zeroes = 0
            for v in values:
                if v == 0:
                    zeroes += 1
            if zeroes > 0:
                log.info(f"{l}, {zeroes = }")

        if args.extended:
            def relative_zero(l, prev, data, offset):
                relative_zeroes = 0
                for i in range(len(data[l][offset:])):
                    if data[l][offset+i] - data[prev][offset+i] <= 0:
                        relative_zeroes += 1
                if relative_zeroes > 0:
                    log.info(f"{l} has {relative_zeroes = }")
                return relative_zeroes

            log.info("extended zero analysis, phase 3")

            # the offset is when we started phase 3
            offset = len(data["2022-04-29/umbrella-top-1000"])
            lists = [
                "2022-04-29/ext-tranco-top-10",
                "2022-04-29/ext-tranco-top-100",
                "2022-04-29/ext-tranco-top-1000",
                "2022-04-29/ext-tranco-top-2000",
                "2022-04-29/ext-tranco-top-4000",
                "2022-04-29/ext-tranco-top-5000",
                "2022-04-29/ext-tranco-top-7000",
                "2022-04-29/ext-tranco-top-10000",
                "2022-04-29/umbrella-top-10000",
            ]
            total_counters = 0
            for l in lists:
                has_zero(data[l][offset:])
                total_counters += len(data[l][offset:])

            relative_zero("2022-04-29/ext-tranco-top-100", "2022-04-29/ext-tranco-top-10", data, offset)
            relative_zero("2022-04-29/ext-tranco-top-1000", "2022-04-29/ext-tranco-top-100", data, offset)
            relative_zero("2022-04-29/ext-tranco-top-2000", "2022-04-29/ext-tranco-top-1000", data, offset)
            relative_zero("2022-04-29/ext-tranco-top-4000", "2022-04-29/ext-tranco-top-2000", data, offset)
            relative_zero("2022-04-29/ext-tranco-top-5000", "2022-04-29/ext-tranco-top-4000", data, offset)
            relative_zero("2022-04-29/ext-tranco-top-7000", "2022-04-29/ext-tranco-top-5000", data, offset)
            relative_zero("2022-04-29/ext-tranco-top-10000", "2022-04-29/ext-tranco-top-7000", data, offset)

            log.info(f"{total_counters = }")
        else:
            log.info("zero analysis of phase 1")
            total_counters = 0

            # when we started phase 3
            max = len(data["2022-04-29/umbrella-top-1000"])

            # how many counters were collected during phase 2, the "15" constant
            # in the end is the exact hour offset to capture when we did the
            # first analysis
            remove = math.floor(len(data["2022-05-20/umbrella-top-1000"]) * (2/3) - (15))

            def relative_zero(l, prev, data):
                relative_zeroes = 0
                for i in range(len((data[l][:max])[:-remove])):
                    if data[l][i] - data[prev][i] <= 0:
                        relative_zeroes += 1
                return relative_zeroes

            relative_map = {}
            for l,v in data.items():
                if is_preload_list(l):
                    has_zero((v[:max])[:-remove])
                    total_counters += len((v[:max])[:-remove])
                    p = prevlist(l, data)
                    if p == "":
                        continue
                    r = relative_zero(l, p, data)
                    relative_map.setdefault(onlylist(l), 0)
                    relative_map[onlylist(l)] = r + relative_map[onlylist(l)]
            
            log.info("")
            zero_map = dict(sorted(relative_map.items(), key=lambda x:x[1]))
            for k,v in zero_map.items():
                log.info(f"{k} {v}")
            log.info("")
            log.info(f"{total_counters = }")

        log.info("")
        total_counters = 0
        for l,v in data.items():
                if is_preload_list(l) and ("ext" in l or "umbrella-top-10000" in l):
                    total_counters += len(v)
                    log.info(f"{l:30} {stats(np.divide(v, lookups[:len(v)]))}")
        log.info(f"in total {total_counters} sample counters for preload lists")


def prevlist(l, list_map):    
    def subprev(l, n):
        for i in range(2*n, 9*n, n):
            if l.endswith(f"top-{i}"):
                return l.replace(f"top-{i}", f"top-{i-n}")
        return ""

    if l.endswith("top-10000"):
        return l.replace("top-10000", "top-9000")
    
    n = subprev(l, 1000)
    if n != "":
        return n
    n = subprev(l, 100)
    if n != "":
        return n
    
    return ""

def onlylist(l):
    p = l.split("/")
    return p[1]

def read(file_path, common_list_id):
    data = {}
    with open(file_path, "r") as fp:
        for line in fp:
            for kv in line.split(" "):
                split = kv.split("=")
                key = fmt(split[0], common_list_id)
                value = int(split[1])

                if key == "timestamp" and value in [ 1652098834, 1652099734 ]:
                    log.warning(f"skip line with timestamp {value} (lists were misconfigured)")
                    break

                data.setdefault(key, [])
                data[key] += [ value ]

    log.info(f"read {len(data['timestamp'])} lines from {file_path} ({dates(data['timestamp'])})")
    return data

def is_preload_list(key):
    return key[0].isnumeric()

def fmt(key, pattern):
    return key[len(pattern):] if key.startswith(pattern) else key

def stats(values):
    minval = np.min(values)
    maxval = np.max(values)
    mean = np.mean(values)
    median = np.median(values)
    return f"min:{minval:.3f} max:{maxval:.3f} mean:{mean:.3f} median:{median:.3f}"

def dates(timestamps):
    start = datetime.datetime.fromtimestamp(timestamps[0])
    end = datetime.datetime.fromtimestamp(timestamps[-1])
    return f"{start} - {end}"

if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s %(name)s [%(levelname)s] %(message)s",
        level=logging.DEBUG,
    )
    parser = argparse.ArgumentParser(
        description="views collected dns metrics"
    )
    parser.add_argument("-p", "--permissive-ports", type=str,
        default="data/DFRI11-ext.dat",
        help="path to data file corresponding to permissive ports",
    )
    parser.add_argument("-w", "--web-ports", type=str,
        default="data/DFRI12-ext.dat",
        help="path to data file corresponding to web ports",
    )
    parser.add_argument("-c", "--common-list-id", type=str,
        default="/etc/tor/tlwo/artifact/preload-lists/generated/",
        help="common list identifier that can be removed",
    )
    parser.add_argument("-e", "--extended", action="store_true",
        default=False,
        help="extended analysis",
    )
    sys.exit(main(parser.parse_args()))
