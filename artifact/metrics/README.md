# Metrics and Data
With the help of [DFRI](https://www.dfri.se/dfri/?lang=en) we collected data for
preload lists on [modified Tor-exits](../exit-relays/README.md). Here, you find
the final graphs for the paper as well as two python files for parsing and
plotting the data.

The data is shared here publicly in filtered form. We have, out of an abundance
of caution, removed the counters for normal Alexa and Tranco lists, as described
in the paper.

If you would like to have access to the raw data as a researcher for research
purposes, please contact Tobias Pulls and explain your use-case.
