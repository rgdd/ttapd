#!/usr/bin/env python3

import sys
import argparse

def main(args):
    if len(args.metrics_at) > 0:
        preload_metrics_option(args.metrics_at)

    # - top 10
    preload_list_option(args.preload_dir, "ext-alexa-top-10")
    preload_list_option(args.preload_dir, "ext-tranco-top-10")
    preload_list_option(args.preload_dir, "alexa-top-10")
    preload_list_option(args.preload_dir, "tranco-top-10")
    preload_list_option(args.preload_dir, "umbrella-top-10")

    # - top 100
    preload_list_option(args.preload_dir, "ext-alexa-top-100")
    preload_list_option(args.preload_dir, "ext-tranco-top-100")
    preload_list_option(args.preload_dir, "alexa-top-100")
    preload_list_option(args.preload_dir, "tranco-top-100")
    preload_list_option(args.preload_dir, "umbrella-top-100")

    # - top 200-1k
    for i in range(100, 1000, 100):
        j = i+100
        preload_list_option(args.preload_dir, f"ext-alexa-top-{j}")
        preload_list_option(args.preload_dir, f"ext-tranco-top-{j}")
        preload_list_option(args.preload_dir, f"alexa-top-{j}")
        preload_list_option(args.preload_dir, f"tranco-top-{j}")
        preload_list_option(args.preload_dir, f"umbrella-top-{j}")

    # - top 2k-10k
    for i in range(1000, 10000, 1000):
        j = i+1000
        preload_list_option(args.preload_dir, f"ext-alexa-top-{j}")
        preload_list_option(args.preload_dir, f"ext-tranco-top-{j}")
        preload_list_option(args.preload_dir, f"alexa-top-{j}")
        preload_list_option(args.preload_dir, f"tranco-top-{j}")
        preload_list_option(args.preload_dir, f"umbrella-top-{j}")

def preload_metrics_option(path):
    print("PreloadMetrics {}".format(path))

def preload_list_option(path, name):
    print("PreloadList {}/{}".format(path, name))

def new_line():
    print("")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="generates torrc options for preload lists",
    )
    parser.add_argument("-d", "--preload-dir",
        required=True, type=str,
        help="directory of preload lists to output torrc options for",
    )
    parser.add_argument("-m", "--metrics-at",
        required=False, type=str, default="",
        help="include torrc option specifying where to write metrics to (file path)",
    )
    sys.exit(main(parser.parse_args()))
