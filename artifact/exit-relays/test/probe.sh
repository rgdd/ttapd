#!/bin/bash

#
# Setup the appropriate circuit and attach all streams:
#
#     $ carml circ --build DFRI0,DFRI11
#     ...outputs CIRC_ID...
#     $ carml stream --attach CIRC_ID
#     ...blocks terminal, attached streams will be shown here...
#     ^C$
#     $ carml circ --delete CIRC_ID
#
# Usage:
#
#     $ ./probe.sh rgdd.se 1000 some-name-$(date +%Y%m%d-%H%M%S)
#
# View the results in FILE:
#
#     $ ./view.py -f FILE
#

set -eu
trap clean EXIT

domain=$(date +%s).$1; shift
num_run=$1; shift
output=$1; shift
mod=25 # print progress every $mod itteration

function main() {
	if [[ -f $output ]]; then
		die_with_error "file $output already exists"
	fi

	want_ip=$(dig +short $domain)
	got_ip=$(tor-resolve $domain)
	if [[ $got_ip != $want_ip ]]; then
		die_with_error "mismatched ip address: $got_ip != $want_ip"
	fi

	info "start $num_run look-ups for $domain with ip $want_ip"
	for i in `seq 1 $num_run`; do
		{ time tor-resolve $i.$domain ; } 2>tmp.time >tmp.ip

		got_ip=$(cat tmp.ip)
		if [[ $got_ip != $want_ip ]]; then
			die_with_error "got ip $got_ip but wanted $want_ip"
		fi

		got_time=$(cat tmp.time | xargs | cut -d' ' -f 2)
		echo $got_time >> $output

		if [[ $i != 0 ]] && [[ $(expr $i % $mod) == 0 ]]; then
			info "$i/$num_run queries completed"
		fi
	done

	info "done, see results in $output"
}

function clean() {
	rm -f tmp.{time,ip}
}

function info() {
	echo "$(date) [INFO] $@" >&2
}

function die_with_error() {
	echo "$(date) [FATA] $@" >&2
	exit 1
}

main
