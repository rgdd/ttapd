#!/usr/bin/env python3

import sys
import argparse
import statistics

def main(args):
    data = []
    with open(args.file, "r") as f:
        for line in f:
            # remove new line
            line = line[:-1]

            # extract number of minutes
            split = line.split("m")
            line = "".join(split[1:])
            time_min = int(split[0])

            # extract number of seconds
            split = line.split(".")
            line = "".join(split[1:])
            time_sec = int(split[0])
            
            # extract number of ms
            time_ms = int(line[:-1])

            # sum up in seconds, save result
            sum_s = 60*time_min + time_sec + time_ms/1000
            data += [ sum_s ]
    
    print("#data:  {}".format(len(data)))
    print("max:    {:.3}s".format(max(data)))
    print("min:    {:.3}s".format(min(data)))
    print("mean:   {:.3}s".format(statistics.mean(data)))
    print("median: {:.3}s".format(statistics.median(data)))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file",
        required=True, type=str,
        help="file with real time results, one per line",
    )
    sys.exit(main(parser.parse_args()))
