#!/usr/bin/env python3

import sys
import argparse

def main(args):
    if len(args.metrics_at) > 0:
        preload_metrics_option(args.metrics_at)

    for i in [ 10, 100, 1000, 2000, 4000, 5000, 7000, 10000 ]:
        preload_list_option(args.preload_dir, f"ext-tranco-top-{i}")

    preload_list_option(args.preload_dir, "umbrella-top-10000")

def preload_metrics_option(path):
    print("PreloadMetrics {}".format(path))

def preload_list_option(path, name):
    print("PreloadList {}/{}".format(path, name))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="generates torrc options for extended preload lists measurements",
    )
    parser.add_argument("-d", "--preload-dir",
        required=True, type=str,
        help="directory of preload lists to output torrc options for",
    )
    parser.add_argument("-m", "--metrics-at",
        required=False, type=str, default="",
        help="include torrc option specifying where to write metrics to (file path)",
    )
    sys.exit(main(parser.parse_args()))
