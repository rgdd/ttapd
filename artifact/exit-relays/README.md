# Exit relays

This file documents changes to the setup of two Tor exit relays that were used
to collect custom [DNS cache performance metrics](../metrics/README.md).  Relay
fingerprints: `937881D3E049BB4E09FE2C742E76ED60C7B6AA3D` (DFRI11) and
`5D8EEBCC17764DD213CD17B9A56844E41EEDA174` (DFRI12).

We're running tor-0.4.6.10 compiled using the same configuration as Debian uses,
but with our [collection patch](./tor-0.4.6.10-metrics.patch) applied.  During
the experiment we upgraded to a newer version, tor-0.4.7.7 (see below).

Both instances run in the same Debian 11 (Bullseye) VM, using the same IP
addresses (both IPv4 and IPv6). The VM has 2 vCPU's and 4G RAM, later increased
to 6G. They both use the same Unbound instance running in a separate VM (on a
different physical host).

**credit:** Linus Nordberg authored this README file.

## Operations timeline

| start date (CEST)   | end date | description                                                  | notes                                                        |
|---------------------|----------|--------------------------------------------------------------|--------------------------------------------------------------|
| 2022-04-19          |          | unbound: install and use it                                  |                                                              |
| 2022-04-19          |          | tor: make sure /usr/bin/tor isn't upgraded by apt            | apt-mark hold tor                                            |
| 2022-04-20          |          | unbound: (mostly) disable RRset caching                      | server: rrset-cache-size: 0                                  |
| 2022-04-21          |          | kernel: disallow ptrace                                      | sysctl kernel.yama.ptrace_scope=2                            |
| 2022-04-22 10:02:53 |          | DFRI12: switch resolver to 171.25.193.228                    | some authoritative nameservers block tor exits               |
| 2022-04-22 11:06:49 | 14:43:20 | DFRI11: not relaying                                         | bad config                                                   |
| 2022-04-22 11:16:29 |          | unbound (now at .228): (mostly) disable RRset caching        | server: rrset-cache-size: 0                                  |
| 2022-04-22 14:43:20 |          | DFRI11: switch resolver to 171.25.193.228                    | some authoritative nameservers block tor exits               |
| 2022-04-22 14:43:20 |          | DFRI11: running patch without preload config                 |                                                              |
| 2022-04-22 14:58:22 |          | DFRI11: preload config AA                                    | AA is a test list                                            |
| 2022-04-22 15:36:26 |          | DFRI12: running patch with preload config AA                 |                                                              |
| 2022-04-22 15:41:30 |          | DFRI11: preloading AA+BB+CC+DD                               |                                                              |
| 2022-04-22 16:14    |          | DFRI{11,12}: running vanilla tor                             | for benchmarking, see test/                                  |
| 2022-04-22 16:21:00 |          | DFRI{11,12}: patched tor with preload config AA              |                                                              |
| 2022-04-25 14:18:43 |          | DFRI{11,12}: preloading 2022-04-22                           | artifact/preload-lists/generated/2022-04-22/, real test list |
| 2022-04-27 09:57:34 |          | unbound: resources increased                                 | ^1                                                           |
| 2022-05-01 12:38    |          | VSZ/RSS DFRI11:2176344/1433804 DFRI12:1003676/779052         | Uptime ~5d22h                                                |
| 2022-05-01 17:02    |          | DFRI{11,12}: restarted                                       | RAM usage control ^2                                         |
| 2022-05-02 13:50    |          | DFRI{11,12}: stopped                                         |                                                              |
| 2022-05-02 13:53    |          | DFRI{11,12}: started with preload config 2022-04-29          | RAM increased 4G -> 6G; testing completed now                |
| 2022-05-02 17:41:04 |          | kernel: TCP: ens2: Driver has suspect GRO implementation     | "TCP performance may be compromised"                         |
| 2022-05-05 00:00:40 | 00:37:08 | TCP performance issues                                       | kernel: TCP: too many orphaned sockets                       |
| 2022-05-05 09:47    | 09:57    | network outage                                               | caused by enp1s0 -> enp2s0 switch                            |
| 2022-05-09 14:16:24 | 14:20:33 | DFRI{11,12}: restarted with new patch and preload 2022-05-06 | see 2022-05-09 new patch, new lists                          |
| 2022-05-09 14:58:41 | 15:04:21 | DFRI{11,12}: restarted with correct 2022-05-06 lists         |                                                              |
| 2022-05-09 17:59    |          | rekursiv0: nft connection tracking disabled for DNS          | we've been seeing warnings for full conntrack table          |
| 2022-05-13 14:52:57 |          | rekursiv0: skip connection tracking altogether               | conntrack table still full                                   |
| 2022-05-16 13:39:56 | 13:40:51 | DFRI{11,12}: restarted with 2022-05-13 lists                 |                                                              |
| 2022-05-23 11:27:46 | 11:28:41 | DFRI{11,12}: restarted with 2022-05-20 lists                 |                                                              |
| 2022-05-30 09:59    |          | DFRI{11,12}: shut down                                       |                                                              |
| 2022-05-30 10:02    |          | kernel: rebooted into 5.10.0-14                              |                                                              |
| 2022-05-30 10:39    |          | DFRI{11,12}: running patched 0.4.7.7 with 2022-05-20 lists   |                                                              |
| 2022-06-12 20:31:16 | 20:31:36 | DFRI{11,12}: restarted with "extended 2022-04-29" lists      | see 2022-06-12 extended lists                                |
| 2022-09-03          |          | DFRI{11,12}: stop data collection                            | added by rgdd 2023-03-06                                     |

The above footnotes:

1. unbound is dropping requests, increasing two configuration options: outgoing-range: 8192 -> 32768; num-queries-per-thread: 4096 -> 16384
2. newly started: VSZ/RSS DFRI11:736968/510008 DFRI12:742652/515652

## Compiling tor

### First patch, late April

This was done on a Debian 11 (Bullseye) system.

```
apt install devscripts build-essential lintian quilt libssl-dev zlib1g-dev libevent-dev pkg-config libcap-dev liblzma-dev libzstd-dev dh-runit dh-apparmor libseccomp-dev libsystemd-dev
apt source tor
cd tor-0.4.6.10
$EDITOR debian/rules # add configure options: --disable-asciidoc --disable-manpage --disable-html-manual
patch -p1 < tor-dns-metrics.patch
debuild -i -us -uc -b -d
...
==> a2x -f manpage debian/tor-instance-create.8.txt
==> make[1]: a2x: No such file or directory
make[1]: *** [debian/rules:61: override_dh_auto_build] Error 127
make[1]: Leaving directory '/tmp/tor-0.4.6.10'
make: *** [debian/rules:23: build] Error 2
...
install -g root -o root -m 755 -ps build/src/app/tor /usr/bin/tor
systemctl restart tor
```

#### Applying collection patch
```
root@mesh4:/tmp/tor-0.4.6.10# sha256sum ~linus/tor-dns-metrics.patch
d572a3e03088cfeb37ccd106e58fdbd935516065f17676c55b7876769e46826b  /home/linus/tor-dns-metrics.patch
root@mesh4:/tmp/tor-0.4.6.10# patch -p1 < ~linus/tor-dns-metrics.patch
patching file src/feature/relay/dns_metrics.c
patching file src/feature/relay/dns_metrics.h
patching file src/feature/relay/include.am
patching file src/app/config/config.c
patching file src/app/config/or_options_st.h
patching file src/feature/relay/dns_metrics.c
patching file src/feature/relay/dns_metrics.h
patching file src/core/or/circuitlist.c
patching file src/core/or/or_circuit_st.h
patching file src/feature/relay/dns.c
patching file src/feature/relay/dns_structs.h
```

### Second patch, 2022-05-09

Tor project, from which we got the source code for 0.4.6.10, has moved
to 0.4.7.7 since we started this experiment. We had to add another APT
source to find 0.4.6.10.

```
root@mesh4:/etc/apt/sources.list.d# cat tpo-nightly-0.4.6.x-bullseye.list
deb https://deb.torproject.org/torproject.org tor-nightly-0.4.6.x-bullseye main
deb-src https://deb.torproject.org/torproject.org tor-nightly-0.4.6.x-bullseye main
```

Fetching source code and applying Debian patches.

```
linus@mesh4:~/usr/src$ apt source tor=0.4.6.10
Reading package lists... Done
NOTICE: 'tor' packaging is maintained in the 'Git' version control system at:
https://git.torproject.org/debian/tor.git
Please use:
git clone https://git.torproject.org/debian/tor.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 8,068 kB of source archives.
Get:1 https://deb.torproject.org/torproject.org tor-nightly-0.4.6.x-bullseye/main tor 0.4.6.10-dev-20220413T210933Z-1~d11.bullseye+1 (dsc) [1,629 B]
Get:2 https://deb.torproject.org/torproject.org tor-nightly-0.4.6.x-bullseye/main tor 0.4.6.10-dev-20220413T210933Z-1~d11.bullseye+1 (tar) [8,007 kB]
Get:3 https://deb.torproject.org/torproject.org tor-nightly-0.4.6.x-bullseye/main tor 0.4.6.10-dev-20220413T210933Z-1~d11.bullseye+1 (diff) [60.0 kB]
Fetched 8,068 kB in 1s (10.3 MB/s)
dpkg-source: info: extracting tor in tor-0.4.6.10-dev-20220413T210933Z
dpkg-source: info: unpacking tor_0.4.6.10-dev-20220413T210933Z.orig.tar.gz
dpkg-source: info: applying tor_0.4.6.10-dev-20220413T210933Z-1~d11.bullseye+1.diff.gz
```

We now have the directory `tor-0.4.6.10-dev-20220413T210933Z` in which
we apply the patch from KAU and compile.

```
cd tor-0.4.6.10-dev-20220413T210933Z/
$EDITOR debian/rules # add configure options: --disable-asciidoc --disable-manpage --disable-html-manual
patch -p1 < /etc/tor/tlwo/artifact/metrics/conf/tor-0.4.6.10-metrics.patch
debuild -i -us -uc -b -d
...
make[1]: a2x: No such file or directory
...
linus@mesh4:~/usr/src/tor-0.4.6.10-dev-20220413T210933Z$ sha256sum ./build/src/app/tor
94e0f27188b96993574af6cc4d53c0471ce282bfd761c31e0e438e4b3dd44e50  ./build/src/app/tor
```

### Upgrading to 0.4.4.7, 2022-05-30

```
linus@mesh4:~/usr/src$ sudo apt-mark hold tor
tor set on hold.
linus@mesh4:~/usr/src$ dpkg -l tor | tail -1
hi  tor            0.4.7.7-1~d11.bullseye+1 amd64        anonymizing overlay network for TCP
```

```
linus@mesh4:~/usr/src$ apt source tor
Reading package lists... Done
NOTICE: 'tor' packaging is maintained in the 'Git' version control system at:
https://git.torproject.org/debian/tor.git
Please use:
git clone https://git.torproject.org/debian/tor.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 7,956 kB of source archives.
Get:1 https://deb.torproject.org/torproject.org bullseye/main tor 0.4.7.7-1~d11.bullseye+1 (dsc) [1,475 B]
Get:2 https://deb.torproject.org/torproject.org bullseye/main tor 0.4.7.7-1~d11.bullseye+1 (tar) [7,895 kB]
Get:3 https://deb.torproject.org/torproject.org bullseye/main tor 0.4.7.7-1~d11.bullseye+1 (diff) [59.7 kB]
Fetched 7,956 kB in 0s (17.9 MB/s)
dpkg-source: info: extracting tor in tor-0.4.7.7
dpkg-source: info: unpacking tor_0.4.7.7.orig.tar.gz
dpkg-source: info: applying tor_0.4.7.7-1~d11.bullseye+1.diff.gz
```

```
linus@mesh4:~/usr/src$ cd tor-0.4.7.7/
linus@mesh4:~/usr/src/tor-0.4.7.7$ vi debian/rules # add configure options: --disable-asciidoc --disable-manpage --disable-html-manual
linus@mesh4:~/usr/src/tor-0.4.7.7$ sha256sum /etc/tor/tlwo/artifact/metrics/conf/tor-0.4.7.7-metrics.patch
70c0a61e379608d1a7c92ab09f20fdc671187e0d52b508be2b88285aea3e9c6a  /etc/tor/tlwo/artifact/metrics/conf/tor-0.4.7.7-metrics.patch
linus@mesh4:~/usr/src/tor-0.4.7.7$ patch -p1 < /etc/tor/tlwo/artifact/metrics/conf/tor-0.4.7.7-metrics.patch
patching file src/feature/relay/dns_metrics.c
patching file src/feature/relay/dns_metrics.h
patching file src/feature/relay/include.am
patching file src/app/config/config.c
patching file src/app/config/or_options_st.h
patching file src/feature/relay/dns_metrics.c
patching file src/feature/relay/dns_metrics.h
patching file src/core/or/circuitlist.c
patching file src/core/or/or_circuit_st.h
patching file src/feature/relay/dns.c
patching file src/feature/relay/dns_structs.h
patching file src/core/or/circuitlist.c
patching file src/core/or/or_circuit_st.h
patching file src/feature/relay/dns.c
patching file src/feature/relay/dns_metrics.c
patching file src/feature/relay/dns_metrics.h
patching file src/feature/relay/dns.c
patching file src/feature/relay/dns_metrics.c
patching file src/feature/relay/dns_metrics.h
linus@mesh4:~/usr/src/tor-0.4.7.7$ time debuild -i -us -uc -b -d
...
make[1]: a2x: No such file or directory
...
user    6m49.165s
...
linus@mesh4:~/usr/src/tor-0.4.7.7$ sha256sum build/src/app/tor
41be08a427d64773cbef2ddba9fa5ecfdeacbabc100e0b9a59ab7b766d3e95be  build/src/app/tor
linus@mesh4:~/usr/src/tor-0.4.7.7$ sudo cp build/src/app/tor /usr/bin/tor
```

### 2022-06-12 extended lists
```
root@mesh4:/etc/tor/instances/171.25.193.235_443# /etc/tor/tlwo/artifact/metrics/conf/torrc-extended.py -m /var/lib/tor-instances/171.25.193.235_443/tlwo-ext.DFRI11.dat -d /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29 > tlwo-preload.conf
root@mesh4:/etc/tor/instances/171.25.193.235_443# wc -l tlwo-preload.conf
10 tlwo-preload.conf
root@mesh4:/etc/tor/instances/171.25.193.235_443# head tlwo-preload.conf
PreloadMetrics /var/lib/tor-instances/171.25.193.235_443/tlwo-ext.DFRI11.dat
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-10
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-100
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-1000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-2000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-4000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-5000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-7000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-10000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/umbrella-top-10000
root@mesh4:/etc/tor/instances/171.25.193.235_443# cd ../171.25.193.235_80/
root@mesh4:/etc/tor/instances/171.25.193.235_80# /etc/tor/tlwo/artifact/metrics/conf/torrc-extended.py -m /var/lib/tor-instances/171.25.193.235_80/tlwo-ext.DFRI12.dat -d /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29 > tlwo-prel
oad.conf
root@mesh4:/etc/tor/instances/171.25.193.235_80# wc -l tlwo-preload.conf
10 tlwo-preload.conf
root@mesh4:/etc/tor/instances/171.25.193.235_80# head tlwo-preload.conf
PreloadMetrics /var/lib/tor-instances/171.25.193.235_80/tlwo-ext.DFRI12.dat
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-10
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-100
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-1000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-2000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-4000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-5000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-7000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-10000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/umbrella-top-10000
```

## Disabling unbound RRset cache

During approximately 20h between Apr 21 and 22, after setting
`rrset-cache-size` to 0, unbound reported ~1% cache hits. Stats were
collected using `unbound-control stats_noreset`.

```
$  echo 12936 1292167 5k/p | dc
.01001
```

## tor configuration

### kau_research.conf
```
root@mesh4:/etc/tor/instances/171.25.193.235_443# date; cat kau_research.conf
Fri 22 Apr 2022 03:00:19 PM CEST
ServerDNSResolvConfFile /etc/tor/instances/171.25.193.235_443/resolv.conf
%include /etc/tor/instances/171.25.193.235_443/tlwo-preload.conf
```

### Generating tlwo-preload.conf
```
root@mesh4:/etc/tor/instances/171.25.193.235_443# python3 /etc/tor/tlwo/artifact/metrics/torrc.py -m /var/lib/tor-instances/171.25.193.235_443/tlwo.DFRI11.dat -d /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-22 > tlwo-preload.conf
root@mesh4:/etc/tor/instances/171.25.193.235_443# head -3 tlwo-preload.conf; tail -1 tlwo-preload.conf; wc -l tlwo-preload.conf
PreloadMetrics /var/lib/tor-instances/171.25.193.235_443/tlwo.DFRI11.dat

PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-22/alexa-top-10
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-22/umbrella-top-10000
65 tlwo-preload.conf
```

### torrc DFRI11 (:443)
```
root@mesh4:/etc/tor/instances/171.25.193.235_443# date; cat torrc; cat kau_research.conf
Mon 25 Apr 2022 03:07:15 PM CEST
# ansible-relayor generated torrc configuration file
# Note: manual changes will be OVERWRITTEN on the next ansible-playbook run

OfflineMasterKey 1
RunAsDaemon 0
Log notice syslog
OutboundBindAddress 171.25.193.235
SocksPort 0
User _tor-171.25.193.235_443
DataDirectory /var/lib/tor-instances/171.25.193.235_443
ORPort 171.25.193.235:443
ORPort [2001:67c:289c:2::235]:443
OutboundBindAddress [2001:67c:289c:2::235]

DirPort 171.25.193.235:8443

SyslogIdentityTag 171.25.193.235_443

ControlSocket 0
CookieAuthentication 0

Nickname DFRI11
ContactInfo DFRI url:https://dfri.se proof:dns-rsa email:tor[]dfri.se abuse:abuse[]dfri.net twitter:dfri_se btc:bc1q24g32dteuu3rnp2vhrecd9rvh4nm30n65n0slm ciissversion:2

NoExec 1

# we are an exit relay!
ExitRelay 1
IPv6Exit 1
DirPort [2001:67c:289c:2::235]:8443 NoAdvertise
DirPortFrontPage /etc/tor/instances/tor-exit-notice.html


ExitPolicy reject 171.25.193.0/24:*
ExitPolicy reject6 [2001:67c:289c::]/48:*
ExitPolicy reject *:25
ExitPolicy reject *:119
ExitPolicy reject *:135-139
ExitPolicy reject *:445
ExitPolicy reject *:563
ExitPolicy reject *:1214
ExitPolicy reject *:4661-4666
ExitPolicy reject *:6346-6429
ExitPolicy reject *:6699
ExitPolicy reject *:6881-6999
ExitPolicy accept *:*

RelayBandwidthRate 12.5 MBytes
ServerDNSAllowBrokenConfig 0

MyFamily 5d8eebcc17764dd213cd17b9a56844e41eeda174,937881d3e049bb4e09fe2c742e76ed60c7b6aa3d
MyFamily DD8BD7307017407FCC36F8D04A688F74A0774C02,A10C4F666D27364036B562823E5830BC448E046A,A478E421F83194C114F41E94F95999672AED51FE,185663B7C12777F052B2C2D23D7A239D8DA88A0F,FFA72BD683BC2FCF988356E6BEC1E490F313FB07,BD6A829255CB08E66FBE7D3748363586E46B3810,32EE911D968BE3E016ECA572BB1ED0A9EE43FC2F,5933473a3563c0666c5bb833c1db553c1f296b74,cf1c1804c33cd69d8a75587fabc63d5d0e2980fa

%include /etc/tor/instances/171.25.193.235_443/kau_research.conf

# end of torrc
ServerDNSResolvConfFile /etc/tor/instances/171.25.193.235_443/resolv.conf
%include /etc/tor/instances/171.25.193.235_443/tlwo-preload.conf
```

### DFRI12 (:80)
```
root@mesh4:/etc/tor/instances/171.25.193.235_80# date; cat torrc; cat kau_research.conf
Mon 25 Apr 2022 03:08:29 PM CEST
# ansible-relayor generated torrc configuration file
# Note: manual changes will be OVERWRITTEN on the next ansible-playbook run

OfflineMasterKey 1
RunAsDaemon 0
Log notice syslog
OutboundBindAddress 171.25.193.235
SocksPort 0
User _tor-171.25.193.235_80
DataDirectory /var/lib/tor-instances/171.25.193.235_80
ORPort 171.25.193.235:80
ORPort [2001:67c:289c:2::235]:80
OutboundBindAddress [2001:67c:289c:2::235]

DirPort 171.25.193.235:8008

SyslogIdentityTag 171.25.193.235_80

ControlSocket 0
CookieAuthentication 0

Nickname DFRI12
ContactInfo DFRI url:https://dfri.se proof:dns-rsa email:tor[]dfri.se abuse:abuse[]dfri.net twitter:dfri_se btc:bc1q24g32dteuu3rnp2vhrecd9rvh4nm30n65n0slm ciissversion:2

NoExec 1

# we are an exit relay!
ExitRelay 1
IPv6Exit 1
DirPort [2001:67c:289c:2::235]:8008 NoAdvertise
DirPortFrontPage /etc/tor/instances/tor-exit-notice.html


ExitPolicy reject 171.25.193.0/24:*,reject6 [2001:67c:289c::]/48:*,accept *:80,accept *:443,reject *:*

RelayBandwidthRate 12.5 MBytes
ServerDNSAllowBrokenConfig 0

MyFamily 5d8eebcc17764dd213cd17b9a56844e41eeda174,937881d3e049bb4e09fe2c742e76ed60c7b6aa3d
MyFamily DD8BD7307017407FCC36F8D04A688F74A0774C02,A10C4F666D27364036B562823E5830BC448E046A,A478E421F83194C114F41E94F95999672AED51FE,185663B7C12777F052B2C2D23D7A239D8DA88A0F,FFA72BD683BC2FCF988356E6BEC1E490F313FB07,BD6A829255CB08E66FBE7D3748363586E46B3810,32EE911D968BE3E016ECA572BB1ED0A9EE43FC2F,5933473a3563c0666c5bb833c1db553c1f296b74,cf1c1804c33cd69d8a75587fabc63d5d0e2980fa

%include /etc/tor/instances/171.25.193.235_80/kau_research.conf

# end of torrc
ServerDNSResolvConfFile /etc/tor/instances/171.25.193.235_80/resolv.conf
%include /etc/tor/instances/171.25.193.235_80/tlwo-preload.conf
```

### 2022-05-09 new patch, new lists

```
root@mesh4:~# systemctl stop tor
root@mesh4:~# sleep 30
root@mesh4:~# install -g root -o root -m 755 -ps ~linus/usr/src/tor-0.4.6.10-dev-20220413T210933Z/build/src/app/tor /usr/bin/tor
root@mesh4:~# sha256sum /usr/bin/tor
9ea8ecbb3dcef160c247710c6a8b83d1a9d81a983346bd475d9d51228c35fac9  /usr/bin/tor

root@mesh4:~# cd /etc/tor/instances/171.25.193.235_443
root@mesh4:/etc/tor/instances/171.25.193.235_443# /etc/tor/tlwo/artifact/metrics/conf/torrc.py -m /var/lib/tor-instances/171.25.193.235_443/tlwo.DFRI11.dat -d /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06 > tlwo-preload.conf

root@mesh4:/etc/tor/instances/171.25.193.235_443# cd /etc/tor/instances/171.25.193.235_80
root@mesh4:/etc/tor/instances/171.25.193.235_80# /etc/tor/tlwo/artifact/metrics/conf/torrc.py -m /var/lib/tor-instances/171.25.193.235_80/tlwo.DFRI12.dat -d /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06 > tlwo-preload.conf

root@mesh4:/etc/tor/instances/171.25.193.235_80# systemctl start tor
```

Double checking the lists.

```
root@mesh4:~# for i in 443 80; do f=/etc/tor/instances/171.25.193.235_$i/tlwo-preload.conf; head -3 $f; tail -3 $f; wc -l $f; sha256sum $f; done
PreloadMetrics /var/lib/tor-instances/171.25.193.235_443/tlwo.DFRI11.dat
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06/ext-alexa-top-10
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06/ext-tranco-top-10
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06/alexa-top-10000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06/tranco-top-10000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06/umbrella-top-10000
101 /etc/tor/instances/171.25.193.235_443/tlwo-preload.conf
fb3b438666ae0392579638a1fc7ec08a54b7a4a2f39d3182f9fdd55e7acad138  /etc/tor/instances/171.25.193.235_443/tlwo-preload.conf
PreloadMetrics /var/lib/tor-instances/171.25.193.235_80/tlwo.DFRI12.dat
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06/ext-alexa-top-10
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06/ext-tranco-top-10
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06/alexa-top-10000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06/tranco-top-10000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06/umbrella-top-10000
101 /etc/tor/instances/171.25.193.235_80/tlwo-preload.conf
540435cfb792ec31fc8165285cf5350724950140870801b8b807fc9556501f55  /etc/tor/instances/171.25.193.235_80/tlwo-preload.conf
```

Wrong config -- we want 04-29 and 05-06, not only 05-06.
New take:
```
/etc/tor/tlwo/artifact/metrics/conf/torrc.py -m /var/lib/tor-instances/171.25.193.235_443/tlwo.DFRI11.dat -d /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29 > tlwo-preload.conf
/etc/tor/tlwo/artifact/metrics/conf/torrc.py -d /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06 >> tlwo-preload.conf

/etc/tor/tlwo/artifact/metrics/conf/torrc.py -m /var/lib/tor-instances/171.25.193.235_80/tlwo.DFRI12.dat -d /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29 > tlwo-preload.conf
/etc/tor/tlwo/artifact/metrics/conf/torrc.py -d /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06 >> tlwo-preload.conf
```

```
root@mesh4:~# for i in 443 80; do f=/etc/tor/instances/171.25.193.235_$i/tlwo-preload.conf; head -3 $f; tail -3 $f; wc -l $f; sha256sum $f; done
PreloadMetrics /var/lib/tor-instances/171.25.193.235_443/tlwo.DFRI11.dat
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-alexa-top-10
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-10
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06/alexa-top-10000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06/tranco-top-10000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06/umbrella-top-10000
201 /etc/tor/instances/171.25.193.235_443/tlwo-preload.conf
d35650aef3a58028675cd08f35c24122d9a117a30c73945aa79245d65c8a163e  /etc/tor/instances/171.25.193.235_443/tlwo-preload.conf
PreloadMetrics /var/lib/tor-instances/171.25.193.235_80/tlwo.DFRI12.dat
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-alexa-top-10
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-04-29/ext-tranco-top-10
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06/alexa-top-10000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06/tranco-top-10000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-06/umbrella-top-10000
201 /etc/tor/instances/171.25.193.235_80/tlwo-preload.conf
6dae550ea7068ca5573adfc0838723df201d0cf08961e55e7d56b5673b4b84b7  /etc/tor/instances/171.25.193.235_80/tlwo-preload.conf
```

### 2022-05-16 new lists
```
root@mesh4:/etc/tor/tlwo# cd /etc/tor/instances/171.25.193.235_443/
root@mesh4:/etc/tor/instances/171.25.193.235_443# /etc/tor/tlwo/artifact/metrics/conf/torrc.py -d /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-13 >> tlwo-preload.conf
root@mesh4:/etc/tor/instances/171.25.193.235_443# wc -l tlwo-preload.conf
301 tlwo-preload.conf
root@mesh4:/etc/tor/instances/171.25.193.235_443# cd ../171.25.193.235_80/
root@mesh4:/etc/tor/instances/171.25.193.235_80# /etc/tor/tlwo/artifact/metrics/conf/torrc.py -d /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-13 >> tlwo-preload.conf
root@mesh4:/etc/tor/instances/171.25.193.235_80# wc -l tlwo-preload.conf
301 tlwo-preload.conf
```

### 2022-05-23 new lists
```
root@mesh4:/etc/tor/instances/171.25.193.235_443# /etc/tor/tlwo/artifact/metrics/conf/torrc.py -d /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-20 >> tlwo-preload.conf
root@mesh4:/etc/tor/instances/171.25.193.235_443# wc -l tlwo-preload.conf
401 tlwo-preload.conf
root@mesh4:/etc/tor/instances/171.25.193.235_443# tail -4 tlwo-preload.conf
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-20/ext-tranco-top-10000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-20/alexa-top-10000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-20/tranco-top-10000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-20/umbrella-top-10000
```
```
root@mesh4:/etc/tor/instances/171.25.193.235_443# cd /etc/tor/instances/171.25.193.235_80
root@mesh4:/etc/tor/instances/171.25.193.235_80# /etc/tor/tlwo/artifact/metrics/conf/torrc.py -d /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-20 >> tlwo-preload.conf
root@mesh4:/etc/tor/instances/171.25.193.235_80# wc -l tlwo-preload.conf
401 tlwo-preload.conf
root@mesh4:/etc/tor/instances/171.25.193.235_80# tail -4 tlwo-preload.conf
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-20/ext-tranco-top-10000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-20/alexa-top-10000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-20/tranco-top-10000
PreloadList /etc/tor/tlwo/artifact/preload-lists/generated/2022-05-20/umbrella-top-10000
```

## Network issues
### Reset adapter unexpectedly
On May 5 around 09:50 CEST we stopped using enp1s0 and started using
enp2s0 due to the driver reporting "Reset adapter unexpectedly" on and
off since May 1 00:34. enp2s0 sits in another port in the switch
(using a separate cable, naturally).

This doesn't seem to have improved things though -- at 15:59 enp2s0
showed the same behaviour.

mesh4 was without network approximately between 09:47 and 09:57 due to this.

```
May  1 00:34:10 unlinkable kernel: [4551978.031581] e1000e 0000:01:00.0 enp1s0: Reset adapter unexpectedly
May  1 01:22:10 unlinkable kernel: [4554858.091797] e1000e 0000:01:00.0 enp1s0: Reset adapter unexpectedly
May  2 08:46:27 unlinkable kernel: [4667917.328000] e1000e 0000:01:00.0 enp1s0: Reset adapter unexpectedly
May  2 13:18:58 unlinkable kernel: [4684268.624523] e1000e 0000:01:00.0 enp1s0: Reset adapter unexpectedly
May  2 13:49:13 unlinkable kernel: [4686083.704195] e1000e 0000:01:00.0 enp1s0: Reset adapter unexpectedly
May  2 17:17:15 unlinkable kernel: [4698565.991929] e1000e 0000:01:00.0 enp1s0: Reset adapter unexpectedly
May  3 22:02:20 unlinkable kernel: [4802072.917590] e1000e 0000:01:00.0 enp1s0: Reset adapter unexpectedly
May  4 02:26:11 unlinkable kernel: [4817904.268736] e1000e 0000:01:00.0 enp1s0: Reset adapter unexpectedly
May  4 06:21:52 unlinkable kernel: [4832045.473644] e1000e 0000:01:00.0 enp1s0: Reset adapter unexpectedly
May  4 21:18:06 unlinkable kernel: [4885820.606300] e1000e 0000:01:00.0 enp1s0: Reset adapter unexpectedly
May  5 06:47:49 unlinkable kernel: [4920004.448325] e1000e 0000:01:00.0 enp1s0: Reset adapter unexpectedly
May  5 07:03:03 unlinkable kernel: [4920918.382255] e1000e 0000:01:00.0 enp1s0: Reset adapter unexpectedly
May  5 07:04:47 unlinkable kernel: [4921022.320268] e1000e 0000:01:00.0 enp1s0: Reset adapter unexpectedly
May  5 07:05:56 unlinkable kernel: [4921091.185652] e1000e 0000:01:00.0 enp1s0: Reset adapter unexpectedly
May  5 15:59:26 unlinkable kernel: [4953102.053417] e1000e 0000:02:00.0 enp2s0: Reset adapter unexpectedly
May  5 16:07:49 unlinkable kernel: [4953605.103235] e1000e 0000:02:00.0 enp2s0: Reset adapter unexpectedly
```

## Random and unsorted notes

### Switching back to vanilla tor
```
root@mesh4:/etc/tor/instances/171.25.193.235_443# ls -l /usr/bin/tor; sha256sum /usr/bin/tor
-rwxr-xr-x 1 root root 3351224 Apr 22 12:29 /usr/bin/tor
f75587622b5cd1f2fc82a089fb1938726512df3e639a19934159cbd8b092aecf  /usr/bin/tor
root@mesh4:/etc/tor/instances/171.25.193.235_443# apt install --reinstall tor
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
0 upgraded, 0 newly installed, 1 reinstalled, 0 to remove and 0 not upgraded.
Need to get 1,985 kB of archives.
After this operation, 0 B of additional disk space will be used.
Get:1 https://deb.torproject.org/torproject.org bullseye/main amd64 tor amd64 0.4.6.10-1~d11.bullseye+1 [1,985 kB]
Fetched 1,985 kB in 1s (3,044 kB/s)
(Reading database ... 95502 files and directories currently installed.)
Preparing to unpack .../tor_0.4.6.10-1~d11.bullseye+1_amd64.deb ...
Unpacking tor (0.4.6.10-1~d11.bullseye+1) over (0.4.6.10-1~d11.bullseye+1) ...
Setting up tor (0.4.6.10-1~d11.bullseye+1) ...
Processing triggers for man-db (2.9.4-2) ...
root@mesh4:/etc/tor/instances/171.25.193.235_443# !ls
ls -l /usr/bin/tor; sha256sum /usr/bin/tor
-rwxr-xr-x 1 root root 3347144 Feb 27 14:21 /usr/bin/tor
7552b79e064f650a8419ad8a7e52b5bebbffea653da8082af70435953585ad50  /usr/bin/tor
```
