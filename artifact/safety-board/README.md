[Final Tor research safety board request.  Change log and timeline at the end.]

# A preload defense and related experiments

This submission is motivated by the responsible disclosure of an attack
involving tor's DNS cache, where the current caching of unpopular domains at
exits lead to potential privacy harms with no utility (i.e., no cache-hits so no
performance gain).

## 1 - The preload defense
This defense gets its name from Mike Perry's suggestion [0].  Here, we refine
the defense, filling in some gaps.  We decided to go down this route since we
think that the logical conclusion of an allow-list defense being viable (as in
our prior submission) leads to this approach, since it's preloading the
allow-list and offers performance gains.

The main part of the defense is to keep the N most popular domains resolved in
the DNS cache by asynchronously resolving them in the background.  All circuits
share this preloaded cache.  The state of the cache must not be influenced by
requests to it (i.e., some async process keeps N domains cached, oblivious to
how the cache is used).

We also permit per-circuit caching: once a domain has been resolved (or is
pending resolution) on a circuit, we let all other streams on the same circuit
use those cache entries.  Once the circuit is closed, the related entries in the
cache are removed.  No sharing or influence across circuits.  This is motivated
by the assumption of multiple concurrent streams to the same destination being
created by Tor Browser.  We want to measure this as part of our proposed
experiments. Maybe we can remove this complexity long-term due to the adoption
of QUIC in HTTP/3 (native in-stream multiplexing).

No other form of DNS caching is done.  This is vital: it forces an attacker who
wishes to use the state of a DNS cache as a side-channel to query the resolver
of the exit.  The exit's resolver is either not directly accessible or shared.
The former occurs if the exit hosts its own resolver.  However, the responsibly
disclosed attack would likely still work most of the time against such a setup.
The latter is promising because a shared cache (e.g., ISP or some DoH provider)
would introduce *false positives* because (non-)Tor domain look-ups are mixed.

For N, assume that we have a list of *domains*, from most to least popular, when
visiting the Tranco top-list with Tor Browser and taking site popularity into
account for the order.  We also need to consider popular subdomains that aren't
captured by Tranco when making this list (e.g., Cisco Umbrella, see proposed
experiments later).

Regarding the value of N, factors to consider:

1.  available memory at the exit for caching,
2.  the distribution of domain popularity, and
3.  acceptable load on resolver.

Addressing 1 is simple: if N is set too high at an exit, just reduce it until in
an acceptable range.  Napkin math tells us that we get very far with tens of MiB
of memory (part of napkin below).  This could be implemented in tor's OOM
manager (without ending up enabling a remote attacker to get N to 0 by filling
circuit caches).

Point 2 is key: it tells us the utility (our cache hit ratio) of different
values of N, and allows us to keep the cache useful.  We expect a powerlaw-like
longtail popularity distribution for domains, just like websites.  Mani et al.
already tell us this with broad strokes [1].  Domain popularity depends on site
popularity (where the domain is used) and number of sites with the domain.  It
would be great if we could generate this list of domains from the Tranco and/or
Cisco Umbrella lists periodically without much additional complexity.

Point 3 ends up being a thing, depending on what TTL we decide to use and N. For
Tranco top-1k, we found 8365 distinct domains to cache after three visits using
Tor Browser with the standard security level (most permissive, so presumably
most domains loaded).  Let's round it up to 10k.  We know that most domains are
currently cached for 5 minutes.  Let's assume all of them are.  This would
result in ``(10×1000)÷(5×60) = 33.333..'' domains to be resolved per second from
the exit as a base-line.  In addition we'd have resolves associated to less
popular websites, which Mani et al. tell us should be about two-thirds of all
traffic at an exit.

Sonntag [2] reports, for a DNS resolver dedicated to a 200 Mbit/s exit in 2018,
an average of 18 requests/s with a maximum of 81 requests/s.  Sonntag, in
another paper [3], shows in Figure 1 a histogram of the number of DNS requests
(thousands) per hour to the DNS resolver.  In table-format:

```
00-20:      10
20-40:      374
40-60:      1268
60-80:      987
80-100:     574
100-120:    268
120-140:    64
140-160:    16
160-180:    8
180-200:    5
200-220:    0
220-240:    1
240-260:    0
260-280:    0
280-300:    1
>=300:      0
```

Returning to our 33.33 requests / second, this gives us about 120k requests per
hour to cache all domains for Tranco top-1k.  Related to Sonntag's figures, this
is quite the increase in resolver traffic.  However, based on Mani et al. and
current TTLs, such extensive caching would see significantly improved cache-hits
on average in the network when compared to today.  Caching only top-100 (1042
distinct domains), we go from caching for ~33% to ~23% of all website visits at
only ~3.5 requests/second.  This span on ~10% of traffic, spanning Tranco
100-1k, is where we need more data to tune.  We also need more data to confirm
that what can be extrapolated from Mani et al. about cache hits is correct.

## 2 - Experiment
### 2.1 - Hypothesis and purpose
Our hypothesis is that for the preload cache defense in the Tor-network, the
necessary list of the most popular domains can be generated based on existing
website- and domain-popularity lists (that are representative for the entire
web/Internet) without negatively impacting cache-hits.

To test our hypothesis, we would like to:

1. Quantify the cache hit ratio for the existing DNS cache design, including if
   cache hits are to 5 or 60 minute TTL "clipped" entries in the cache.  This is
   our baseline to compare against.

2. Quantify the cache hit ratio *on the same circuit*.  Currently, multiple
   streams that connect to the same domain share the same cache (because global)
   and *pending resolves* in the cache. If this is rare (or something we think
   will become increasingly rare due to the wider use of QUIC once tor has UDP
   support) we could simplify the design by removing same-circuit caching.

3. Quantify the cache hit ratio for the preload cache defense based on different
   lists of domains derived from popularity lists.  We want to check the Alexa
   list because that is what Mani et al. used [4], the newer Tranco list because
   it may be more accurate [5], and the Cisco Umbrella list that also contains
   "non-web" domains [6].  We also want to check how changes to the underlying
   popularity lists affect cache hit ratios due to gradually becoming outdated.

To collect data, we propose running modified exit relays that collect metrics.

### 2.2 - Data collection
Every 15 minutes, we collect the values of the following counters:

- `uint64 timestamp`: the UNIX timestamp when the data was collected.

- `uint64 lookups`: the total number of observed domain look-ups, i.e., cache
  hits and misses.

- `uint64 num_cache_entries` the total number of entries in tor's existing DNS
  cache (sum of pending and cached entries), included for sake of comparison.

- `uint64 num_same_circuit_entries` the total number of entries in all the same
  circuit caches (sum), included for sake of accurately being able to compare
  the memory usage with tor's existing DNS cache (in terms of the total number
  of entries in caches needed).

- `uint64 hits_5m`: the number of cache hits with a 5m TTL.
- `uint64 hits_60m`: the number of cache hits with a 60m TTL.
- `uint64 hits_pending`: the number of cache hits with a pending resolve.

- `uint64 hits_$domain_list` for each `$domain_list`: the number of cache hits
	that would have ocurred if `$domain_list` was preload cached.

- `uint64 hits_same_circuit` the number of streams that have looked up a domain
  that was previously looked up on the same circuit.

All counters are zeroed after collection (so every 15 minutes).  Counters are
kept in memory.  Upon collection, their values are stored to disk.

### 2.3 - Setup and duration
We propose operating two modified Tor relays with identical bandwidth, hardware,
location, and configuration.  The goal is to have the same exit probability. Our
relays will be operated by DFRI (https://www.dfri.se/dfri/?lang=en), a Swedish
non-profit and non-partisan organisation that promotes digital rights.  DFRI
already operates several high-capacity exits in the Tor network.  We will have
no access to the relays. This is essential because we already operate default
bridges and the same operator should not be involved in both.

We want to run two relays, where one relay will only have web ports enabled in
its exit policy, and the other relay will be more permissive to also serve
"non-web" traffic.  We will keep our measurement running for four weeks.  Each
relay will have its own DNS resolver running locally.  This removes any TTL bias
due to non-tor traffic resulting in caching at the resolver.

After completion, the relays will be wiped.  Only the collected values,
described in the earlier section, will be saved from the relays.

### 2.4 - Lists of domains
Multiple lists are constructed based on Alexa, Cisco Umbrella, and Tranco, then
split as follows:

	- top 10
	- top 100
	- top [200, 1k], split steps of 100
	- top [2k, 10k], split steps of 1k

In total, there are `20x3 + 20x2 = 100` lists.  First, `20x3 = 60` lists are
from Alexa, Cisco Umbrella, and Tranco.  Then, the other `20x2 = 40` lists are
based on *extended* Alexa and Tranco lists.  The extended lists contain contain
all domains resolved when visiting each website on the list.

We think top-10k is reasonable here, because with 5 minutes TTL a rough upper
limit on resolver load was at top 1k (see end of Section 1). If we increase the
TTL to an hour, we could hold about an order of magnitude more domains with
similar resolver load.

Our measurements will run for four weeks.  At the start of every week, we will
create another set of 100 lists based on refreshed popularity lists.  This gives
us the first week of 100 lists, the second week of 200 lists, and the third week
of 300 lists, and the fourth and final week of 400 lists.

### 2.5 - What we will learn
In addition to answering the questions in Section 2.1, we will learn the number
of DNS lookups that are performed on an exit with a given exit probability. This
can be extrapolated to the entire network, and even say something about the
estimated number of website visits in the network as a function of time.

### 2.6 - Why this is safe
In terms of how we collect data, our modifications to tor involve adding
counters in relation to domain lookups.  Such a change is modest because it
involves relatively few changes to the overall codebase.  The modified versions
of tor are run on machines under control, as part of an environment that has so
far served the Tor Project well by providing relays to the network for many
years.  As part of other research, we also have a history of running modified
versions of tor on the network without incidents (albeit non-exits).

With a focus on the collected data and the resulting dataset, we aggregate both
in terms of time and number of domains for each collected counter. 

The timespan of each measurement is set to 15 min.  Considering only website
visits, Mani et al. puts this to about 1.45 million website visits in the
network.  Assuming an exit probability of 0.05% (based on exits with the same
target 100 mbit/s of bandwidth), this is approximately 725 website visits at one
of our exits.  We store these visits into a number of counters.

The number of domains per counter follows the popularity lists, where the most
popular domains are the fewest (10), but represent the most popular
websites/domains on the web/Internet.  As popularity drops, the number of
domains per counter greatly increases.  Prior research by Mani et al. tells us
that popularity lists match the traffic observed in the Tor network, so our
granularity is a function of the expected utility for an attacker.

We increase the resolution beyond that of Mani et al. in the top (100, 1k] and
(1k, 10k] ranges.  However, here each bucket consists of 100/1k domains (about
an order more for sites on the extended Alexa and Tranco lists).

The most utility for an attacker is to learn that no visit occurred from our
exits as indicated by zero counters.  However: the combination of 15m timespans,
increasingly large buckets for all counters, and our limited network visibility
(~0.1%) makes the data set an unreliable source for confirmation or correlation.

### 2.7 - Why this outweighs the risks
The risk is small: the most risky part is to operate modified exits.  The
resulting dataset has little to no use in confirmation or correlation attacks.
Therefore, we believe that it should be made public exactly as collected by us.

The benefits are significant: the current DNS cache design is a privacy harm, as
our responsibly disclosed attack shows.  As-is, an attacker can use the state of
the DNS caches in the network to create highly precise correlation and
confirmation attacks.  We have good reasons to believe that most domains cached
today in the network serve no other purpose than harming user privacy, i.e.,
with little to no utility in terms of cache hits.  Change is needed.  If the
proposed defense is performant enough, it would significantly improve the status
quo if deployed.  Understanding the performance impact is vital for deployment.

## Changelog and timeline
- 17 June: discuss impact of collected zero counters in non-extended Alexa and
  Tranco lists.  The conclusion was to remove them from the public data set,
  erring on the side of safety.  Researchers may request access to all data.
- 30 May: request feedback on extending the 4-week measurement over the summer,
  purpose to see when the the oldest preload lists become non-performant.
- 5 May 2022: Added the collection of the total number of same-circuit DNS cache
  entries to more accurately be able to compare memory usage.
- 3 May 2022: minor updates at the start of measurement to incorporate changes
  made on earlier feedback and experience during trial runs.  Clarified that our
  lists are complete top lists, including raw/original Alexa and Tranco lists,
  as well as extended lists with domains from website visits.  This increases
  our total number of lists from 240 to 400.  Clarified that DFRI is operating
  the modified relays for the experiment to prevent us from operating both exits
  and bridges concurrently.  Added the collection of the total number of entries
  in tor's existing DNS cache for sake of comparing memory usage.
- 2 February 2022: significantly refactored our initial proposal into what's
  largely here today.  Triggered by valuable feedback from Marc Juarez and Georg
  Koppen.

## References
0. https://gitlab.torproject.org/tpo/core/tor/-/issues/32678#note_2729714
1. Akshaya Mani, T. Wilson-Brown, Rob Jansen, Aaron Johnson, and Micah Sherr.
    "Understanding Tor Usage with Privacy-Preserving Measurement", IMC 2018
2. Michael Sonntag, "DNS Traffic of a Tor Exit Node - An Analysis", SpaCCS
    2018, https://link.springer.com/chapter/10.1007/978-3-030-05345-1_3
3. Michael Sonntag, "Malicious DNS Traffic in Tor: Analysis and
    Countermeasures", ICISSP 2019,
    https://www.scitepress.org/Papers/2019/74712/74712.pdf
4. Mani et al., "Understanding Tor Usage with Privacy-Preserving Measurement",
    IMC 2018
5. Le Pochat et al., "Tranco: A Research-Oriented Top Sites Ranking Hardened
    Against Manipulation", NDSS 2019
6. Cisco, "Umbrella Popularity List",
    http://s3-us-west-1.amazonaws.com/umbrella-static/index.html
