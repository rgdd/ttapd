#!/usr/bin/env python3
import argparse
import zipfile
import os
import sys

ap = argparse.ArgumentParser()
ap.add_argument("-a", required=True,
    help="alexa zip")
ap.add_argument("-t", required=True,
    help="tranco zip")
ap.add_argument("-u", required=True,
    help="umbrella zip")

ap.add_argument("-l", required=True, 
    help="unique sites list")
ap.add_argument("-c", nargs="+", required=True, 
    help="collected domains")

ap.add_argument("-s", required=True, 
    help="save generated list to the following folder")

ap.add_argument("-n", required=False, type=int, default=10000,
    help="maximum site/domain popularity to consider")
args = vars(ap.parse_args())

site2domains = {}
uniquesites = []

def main():
    if os.path.exists(args["s"]):
        sys.exit(f"save folder {args['s']} already exists")
    os.makedirs(args["s"], exist_ok=True)

    with open(args["l"], "r") as f:
        for line in f:
            uniquesites.append(line.strip())
    print(f"loaded {len(uniquesites)} unique sites")

    for l in args["c"]:
        loadcollected(l)
    print(f"loaded domains from {len(args['c'])} collect runs")

    # sanity check
    zeroes = []
    singles = []
    for site, domains in site2domains.items():
        if len(domains) == 0:
            zeroes.append(site)
        elif len(domains) == 1:
            singles.append(site)
    print(f"got {len(zeroes)} zeroes")
    print(f"got {len(singles)} singles")

    # for alexa/tranco
    alexa = loadlist(args["a"])
    tranco = loadlist(args["t"])
    umbrella = loadlist(args["u"])

    # Multiple lists are constructed based on Alexa, Cisco Umbrella, and Tranco,
    # then split as follows:
	
    # - top 10
    save(top_web(alexa, 0, 10), "ext-alexa-top-10")
    save(top_web(tranco, 0, 10), "ext-tranco-top-10")
    save(alexa[0:10], "alexa-top-10")
    save(tranco[0:10], "tranco-top-10")
    save(umbrella[0:10], "umbrella-top-10")
	
    # - top 100
    save(top_web(alexa, 0, 100), "ext-alexa-top-100")
    save(top_web(tranco, 0, 100), "ext-tranco-top-100")
    save(alexa[0:100], "alexa-top-100")
    save(tranco[0:100], "tranco-top-100")
    save(umbrella[0:100], "umbrella-top-100")

	# - top 200-1k
    for i in range(100, 1000, 100):
        j = i+100
        save(top_web(alexa, 0, j), f"ext-alexa-top-{j}")
        save(top_web(tranco, 0, j), f"ext-tranco-top-{j}")
        save(alexa[0:j], f"alexa-top-{j}")
        save(tranco[0:j], f"tranco-top-{j}")
        save(umbrella[0:j], f"umbrella-top-{j}")

	# - top 2k-10k
    for i in range(1000, 10000, 1000):
        j = i+1000
        save(top_web(alexa, 0, j), f"ext-alexa-top-{j}")
        save(top_web(tranco, 0, j), f"ext-tranco-top-{j}")
        save(alexa[0:j], f"alexa-top-{j}")
        save(tranco[0:j], f"tranco-top-{j}")
        save(umbrella[0:j], f"umbrella-top-{j}")

def save(list, fname):
    with open(os.path.join(args["s"], fname), "w") as w:
        for domain in list:
            w.write(f"{domain}\n")

# unique domains for top (lo,hi]
def top_web(list,lo,hi):
    unique = []
    sub = list[lo:hi]
    for site in sub:
        for d in site2domains[site]:
            if d not in unique:
                unique.append(d)
    unique.sort()
    return unique

def loadcollected(l):
    with open(l, "r") as f:
        for i, line in enumerate(f):
            site = uniquesites[i]
            if site not in site2domains:
                site2domains[site] = []
            if "," in line:
                domains = site2domains[site]
                p = line.split(",")
                for j in range(len(p)-1):
                    d = p[j+1].strip()
                    if d not in domains:
                        domains.append(d)
                site2domains[site] = domains

def loadlist(fname):
    poplist = []
    with zipfile.ZipFile(fname) as z:
            with z.open('top-1m.csv') as c:
                for line in c:
                    p = line.decode('utf-8').strip().split(",")
                    if int(p[0]) > args["n"]:
                        break
                    poplist.append(p[1])
    return poplist

if __name__ == "__main__":
    main()
