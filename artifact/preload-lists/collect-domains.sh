# unique list of sites
LIST=unique-domains-10k

# 3x from Europe
mullvad relay set location se sto
echo "set location to Stockholm"
sleep 5
time ./preload-collect-domains.py -l $LIST -n 3 -b 60 -t 20
sleep 10
mv results.csv results-eu-3.csv
cp results-eu-3.csv ~/Downloads/

# 2x from US
sleep 5
mullvad relay set location us nyc
echo "set location to New York"
sleep 10
time ./preload-collect-domains.py -l $LIST -n 2 -b 60 -t 20
mv results.csv results-us-2.csv
cp results-us-2.csv ~/Downloads/

# 2x from HK
sleep 5
mullvad relay set location hk
echo "set location to Hong Kong"
sleep 10
time ./preload-collect-domains.py -l $LIST -n 2 -b 60 -t 20
mv results.csv results-hk-2.csv
cp results-hk-2.csv ~/Downloads/

mullvad relay set location se got
echo "set location to Göteborg"
