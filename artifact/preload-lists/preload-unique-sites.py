#!/usr/bin/env python3
import argparse
import zipfile
import random

ap = argparse.ArgumentParser()
ap.add_argument("-l", nargs="+", required=True, 
    help="popularity lists to parse")

ap.add_argument("-n", required=False, type=int, default=10000,
    help="number of sites")
args = vars(ap.parse_args())

def main():
    unique = {} # dict for faster lookup
    for l in args["l"]:
        print(l)
        with zipfile.ZipFile(l) as z:
            with z.open('top-1m.csv') as c:
                for line in c:
                    p = line.decode('utf-8').strip().split(",")
                    if int(p[0]) <= args["n"] and p[1] not in unique:
                        unique[p[1]] = True

    domains = list(unique.keys())
    random.shuffle(domains)
    fname = f"unique-domains"
    with open(fname, "w") as f:
        for d in domains:
            f.write(f"{d}\n")

    print(f"wrote {len(unique)} unique domains to \"{fname}\" in random order")

if __name__ == "__main__":
    main()
