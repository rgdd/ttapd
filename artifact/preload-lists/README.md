# Preload lists

Resulting lists used at exits in `generated/`.

## Weekly list generation procedure

### Friday
Download fresh Alexa, Tranco, and Umbrella lists. Store in `lists/`. Make a list
of unique domains for sites to visit:

```
./preload-unique-sites.py -l popularity/2022-04-22-alexa-top-1m.zip popularity/2022-04-22-tranco_LY544-1m.csv.zip
```

Store the resulting unique file as `unique-sites-10k` in a dedicated subfolder
of `collected/`.

### Saturday/Sunday
For all unique sites, collect domains from Europe x3 (Stockholm), US x2 (New
York), and Hong Kong x2. See below for collection details.

Copy the result files with collected domains into the `collected/` folder.

Generate new preload lists, store in `generated/`. 

```
./preload-gen-list.py -a lists/2022-04-22-alexa-top-1m.zip -t lists/2022-04-22-tranco_LY544-1m.csv.zip -u lists/2022-04-22-umbrella.zip -l collected/2022-04-22/unique-domains-10k -c collected/2022-04-22/results-eu-3.csv collected/2022-04-22/results-us-2.csv collected/2022-04-22/results-hk-2.csv -s generated/2022-04-22/
```

### Monday 
Add new lists to exits.

## Collection Details

What takes time is collection. We use headless Chromium (Version 100.0.4896.127
(Official Build) snap (64-bit)).

First, create a ramdisk (we'll be smashing disk):

```
sudo mount -t tmpfs -o rw,size=2G tmpfs /mnt/ramdisk
```

Copy over `preload-collect-domains.py`, `collect-domains.sh`, and the list of
unique domains/sites to visit `unique-domains-10k`.

Make a small version of the list (e.g., 100 sites) for testing. Install Mullvad
VPN client (or modify `collect-domains.sh` to use whatever you prefer).

Run `./collect-domains.sh` and sit back. Collection time for 10k domains is
about 9 hours over night with default parameters (memory can spike quite a lot
on some sites). 

When done, three csv-files with results are written to the ramdisk and copied to
`~/Downloads/` (learned the hard way!): `results-eu-3.csv`, `results-hk-2.csv`,
`results-us-2.csv`.

### Why Chromium on Clearnet/VPN and not TB?
Because we think we get more domains this way. Sure, we might be caching domains
for sites that are completely blocking Tor, but it doesn't hurt our findings.
Chromium is also ``very liberal'' in all the creative content of the web it
loads.

### Why 3+2+2 Collection Rounds from EU/US/HK?
We started with five rounds, and found that the most important aspect was
actually geographic location on the number of total unique domains observed (in
general, more advertisement crap loaded from the US it seems?). Since our
collection approach with headless Chromium is pretty lazy (fire up and forget),
we wanted some repetitions (so at least twice per region), and including a place
in Asia seemed sound in addition to EU/US where Tor-exits typically located.

Inspecting domains, we currently collect a lot of advertisement domains. For
example, a ton of subdomain.safeframe.googlesyndication.com: this is probably a
bad idea, since subdomains for safeframes
(0e06fa9953a4b314a68327c74f508e1a.safeframe.googlesyndication.com) are
supposedly random. Oh well.

So, plenty of room to improve here.