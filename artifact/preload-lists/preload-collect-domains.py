#!/usr/bin/env python3
import argparse
import os
import subprocess
import tldextract
import numpy as np
import datetime
import iptools

ap = argparse.ArgumentParser()
ap.add_argument("-l",required=True, 
    help="list of domains")
ap.add_argument("-n", required=True, type=int,
    help="number of repetitions")

ap.add_argument("-b", required=False, type=int, default=10,
    help="batch size")
ap.add_argument("-t", required=False, type=int, default=10,
    help="timeout for each attempted visit")
args = vars(ap.parse_args())

def now():
    return datetime.datetime.now().strftime("%H:%M:%S")

# results: list index of site -> samples x [observed domains visiting site]
results = {}
randomIDs = []
totaldomains = 0

def main():
    global randomIDs

    sites = []
    with open(args["l"], "r") as f:
        for s in f:
            if len(s) > 0:
                sites.append(s.strip())
    print(f"list has {len(sites)} sites")

    for i in range(args["n"]):
        print(f"{now()} round {i+1}/{args['n']}")
        randomIDs = np.random.permutation(len(sites))
        visits = 0
        while visits < len(sites):
            collect(sites, visits)
            visits += args["b"]

    with open("results.csv", "w") as w:
        for i in range(len(sites)):
            w.write(f"{i}")
            for d in results[i]:
                w.write(f",{d}")
            w.write("\n")

def collect(allsites, n):
    global randomIDs, totaldomains
    indices = []
    if n+args["b"] <= len(allsites):
        indices = randomIDs[n:n+args["b"]]
    else:
        indices = randomIDs[n:]

    sites = []
    for i in range(len(indices)):
        sites.append(allsites[randomIDs[indices[i]]])

    print(f"{now()}\tcollected {n:5d}/{len(allsites)} sites, {totaldomains:6d} unique domains, starting from {sites[0]}")

    lazyrun(sites)
    parse(sites, indices)
    cleanup(sites)

def parse(sites, indices):
    global results, randomIDs, totaldomains
    for i in range(len(sites)):
        resultindex = randomIDs[indices[i]]
        if resultindex not in results:
            results[resultindex] = []
        fname = f"{i}.json"
        if os.path.exists(fname):
            with open(fname, "r") as f:
                for line in f:
                    if "dns_query" in line and '"host":' in line:
                        d = getdomain(line)
                        if d not in results[resultindex] and d != "":
                            domains = results[resultindex]
                            domains.append(d)
                            totaldomains += 1
                            results[resultindex] = domains
    return results

def getdomain(line):
    domain = ""
    lo = line.find('"host":')
    if lo != -1:
        hi = line[lo+9:].find('"')
        if hi != -1:
            d = tldextract.extract(line[lo+8:lo+9+hi])
            if len(d.subdomain) > 0:
                domain = f"{d.subdomain}.{d.domain}.{d.suffix}"
            else:
                domain = f"{d.domain}.{d.suffix}"

    if isip(domain) or isip(domain[:-1]):
        return ""
    return domain

def isip(ip):
    return iptools.ipv4.validate_ip(ip)

CHROMERUN = "chromium --disable-gpu --headless --incognito-mode "\
            "--screenshot={}.png --window-size=1280,1696 --log-net-log={}.json " \
            "--net-log-level=0 --net-log-level=0 --virtual-time-budget=5000 http://{} &\n"
LAZYRUN = "/bin/bash lazy.sh"

def lazyrun(sites):
    with open("lazy.sh", "w") as w:
        for i, site in enumerate(sites):
            w.write(CHROMERUN.format(i, i, site))
        w.write(f"sleep {args['t']}\n")
        w.write("killall chrome\n") # HERE BE DRAGONS
    subprocess.run(LAZYRUN, capture_output=False, text=False, shell=True,
    stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

def cleanup(sites):
    with open("lazy.sh", "w") as w:
        for i, _ in enumerate(sites):
            w.write(f"rm {i}.png {i}.json\n")
    subprocess.run(LAZYRUN, capture_output=False, text=False, shell=True,
    stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

if __name__ == "__main__":
    main()
